﻿using System;
using UnityEngine;

namespace RayFire
{
	[Serializable]
	public class RFShatterProperties
	{
		[Tooltip ("Editor: Allows to fragment complex multi element hi poly meshes with topology issues like open edges and unwelded vertices.")]
		public FragmentMode mode;
		[Range (0, 100)] public int seed;
        public bool decompose;
        public bool removeCollinear;
        public bool copyComponents;
        public bool excludeInnerFragments;

        /// /////////////////////////////////////////////////////////
        /// Constructor
        /// /////////////////////////////////////////////////////////
		
		// Constructor
		public RFShatterProperties()
		{
			mode                  = FragmentMode.Editor;
			seed                  = 1;
			decompose             = false;
			removeCollinear       = true;
			copyComponents        = false;
			excludeInnerFragments = false;
		}
	}
}
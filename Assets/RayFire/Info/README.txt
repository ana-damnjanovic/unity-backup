RayFire for Unity build v. 1.17

---------------------------------------

Website: 
http://rayfirestudios.com/

Discord:
https://discord.gg/8G98JKj

YouTube: 
https://www.youtube.com/user/MirVadim

Email:   
unity@rayfirestudios.com

Skype:
mir-vadim

Unity forum:
https://forum.unity.com/threads/rayfire-for-unity-runtime-demolition-and-fragmentation-plugin.674593/

Twitter:
https://twitter.com/RayFireStudios

Facebook:
https://www.facebook.com/groups/277693911374/

Trello:  
https://trello.com/b/ZilFOIoY/rayfire-for-unity

Forum:
http://rayfirestudios.com/forum/

---------------------------------------

Unity builds support: 2018, 2019

---------------------------------------

Platforms with runtime fragmentation support: Windows, Mac OS X

Support for other platforms will be added over time.

Already prefragmented objects can be simulated and demolished on any build platforms.

---------------------------------------

How to test runtime demolition feature:
Open scene in Assets/RayFire/Tutorial/Scenes/2.3 Rigid. Mesh. Demolition Types
Start Play Mode
Rock should fall down and demolish concrete plates.
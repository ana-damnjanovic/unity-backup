Character Movement Fundamentals V1.1

Changelog:

Version 1.1

- Replaced old showcase scene with new version (now with a proper controller selection menu).
- Polished controller prefab settings.
- Replaced 'ApplicationControl' and 'ChangeControllerType' scripts with new 'DemoMenu' script, which will now handle switching between different controller prefabs.
- Added 'DisableShadows', 'FPSCounter' and 'PlayerData' scripts, all of which are used in the new showcase scene.
- Decreased scene light intensity to '0.7'.
- Removed all (unnecessary) 'Showcase' controller prefabs, all example scenes now use the controller prefabs included in the projects.
- Added 'MouseCursorLock' script, which provides basic mouse cursor locking functionality. Old (unstable) 'FocusMouse' script was removed.
- Updated user manual to reflect changes.

Version 1.0

- Initial release


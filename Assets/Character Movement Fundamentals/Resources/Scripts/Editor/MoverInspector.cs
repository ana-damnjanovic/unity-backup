﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Reflection;

//This editor script displays the inspector GUI of the mover components;
//It also updates the collider dimensions whenever a value is changed in the inspector;
[CustomEditor(typeof(Mover))]
public class MoverInspector : Editor {

	private Mover mover;

	private string[] physicsLayers;

	private Vector3[] raycastArrayPositions;

	void Start()
	{
		Setup();
	}

	void Reset()
	{
		Setup();
	}

	void OnEnable()
	{
		Setup();
	}

	void Setup()
	{
		mover = (Mover)target;

		List<string> layers = new List<string>();

		for(int i = 0; i < 32; i ++)
		{
			layers.Add(LayerMask.LayerToName(i));
		}

		physicsLayers = layers.ToArray();

		raycastArrayPositions = Sensor.GetRaycastStartPositions(mover.sensorArrayRows, mover.sensorArrayRayCount, mover.sensorArrayRowsAreOffset, 1f);
	}

	public override void OnInspectorGUI()
	{
		if(mover == null)
		{
			Setup();
			return;
		}

		GUILayout.Label("Mover Options", EditorStyles.boldLabel);

		Rect space;

		EditorGUI.BeginChangeCheck();

		mover.stepHeight = EditorGUILayout.Slider("Step Height", mover.stepHeight, 0f, 1f);

		GUILayout.Label("Collider Options", EditorStyles.boldLabel);

		mover.colliderHeight = EditorGUILayout.FloatField("Collider Height", mover.colliderHeight);
		mover.colliderThickness = EditorGUILayout.FloatField("Collider Thickness",mover.colliderThickness);
		mover.colliderOffset = EditorGUILayout.Vector3Field("Collider Offset", mover.colliderOffset);


		if(EditorGUI.EndChangeCheck())
		{
			mover.RecalculateColliderDimensions();
		}

		GUILayout.Label("Sensor Options", EditorStyles.boldLabel);

		mover.sensorType = (Sensor.CastType)EditorGUILayout.EnumPopup("Sensor Type", mover.sensorType);
		mover.sensorLayermask = EditorGUILayout.MaskField("Layermask", mover.sensorLayermask, physicsLayers);

		mover.isInDebugMode = EditorGUILayout.Toggle("Debug Mode",mover.isInDebugMode);

		if(mover.sensorType == Sensor.CastType.RaycastArray)
			GUILayout.Label("Advanced Options", EditorStyles.centeredGreyMiniLabel);
		GUILayout.Space(5);

		if(mover.sensorType == Sensor.CastType.Raycast)
		{
		}
		else if(mover.sensorType == Sensor.CastType.Spherecast)
		{

		}
		else if(mover.sensorType == Sensor.CastType.RaycastArray)
		{
			if(raycastArrayPositions == null)
				raycastArrayPositions = Sensor.GetRaycastStartPositions(mover.sensorArrayRows, mover.sensorArrayRayCount, mover.sensorArrayRowsAreOffset, 1f);

			EditorGUI.BeginChangeCheck();

			mover.sensorArrayRayCount = EditorGUILayout.IntSlider("Number", mover.sensorArrayRayCount, 3, 9);
			mover.sensorArrayRows = EditorGUILayout.IntSlider("Rows", mover.sensorArrayRows, 1, 5);
			mover.sensorArrayRowsAreOffset = EditorGUILayout.Toggle("Offset Rows", mover.sensorArrayRowsAreOffset);

			if(EditorGUI.EndChangeCheck())
			{
				raycastArrayPositions = Sensor.GetRaycastStartPositions(mover.sensorArrayRows, mover.sensorArrayRayCount, mover.sensorArrayRowsAreOffset, 1f);
			}

			GUILayout.Space(5);

			space = GUILayoutUtility.GetRect(GUIContent.none, GUIStyle.none, GUILayout.Height(100));

			Rect background = new Rect(space.x + (space.width - space.height)/2f, space.y, space.height, space.height);
			EditorGUI.DrawRect(background, Color.grey);

			float point_size = 3f;

			Vector2 center = new Vector2(background.x + background.width/2f, background.y + background.height/2f);

			if(raycastArrayPositions != null && raycastArrayPositions.Length != 0)
			{
				for(int i = 0; i < raycastArrayPositions.Length; i++)
				{
					Vector2 position = center + new Vector2(raycastArrayPositions[i].x, raycastArrayPositions[i].z) * background.width/2f * 0.9f;

					EditorGUI.DrawRect(new Rect(position.x - point_size/2f, position.y - point_size/2f, point_size, point_size), Color.white);
				}
			}

			if(raycastArrayPositions != null && raycastArrayPositions.Length != 0)
				GUILayout.Label("Number of rays = " + raycastArrayPositions.Length, EditorStyles.centeredGreyMiniLabel );
		}

		EditorUtility.SetDirty(mover);
	}

}

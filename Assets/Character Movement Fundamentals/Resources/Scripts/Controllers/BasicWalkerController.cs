﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Basic controller script;
//This controller is used as a basis for more advanced controller types ('SidescrollerController', 'CameraWalkerController');
//Custom movement input can be implemented by creating a new script that inherits 'BasicWalkerController' and overrides the 'CalculateMovementDirection' function;
public class BasicWalkerController : MonoBehaviour {

	//References to attached components;
	private Transform tr;
	private Mover mover;

	//Names of input axes used for horizontal and vertical input;
	public string horizontalInputAxis = "Horizontal";
	public string verticalInputAxis = "Vertical";

	//Whether or not to use raw input values;
	public bool useRawInput = false;

	//Keycode used for jumping;
	public KeyCode jumpKey = KeyCode.Space;

	//Whether the jump key was pressed during last Update frame;
	bool jumpKeyWasPressed = false;

	//Movement speed;
	public float movementSpeed = 7f;

	//'Aircontrol' determines to what degree the player is able to move while in the air;
	[Range(0f, 1f)]
	public float airControl = 0.4f;

	//Jump speed;
	public float jumpSpeed = 15f;

	// 5ms leniency to allow the player to jump after losing ground contact
	[Tooltip("Jump leniency in milliseconds.")]
	public float jumpLeniency = 5f;
	private float jumpLeniencyInSeconds;

	//'Friction' determines how fast the player loses (horizontal) momentum while in the air;
	public float friction = 0.5f;

	//Current momentum;
	Vector3 momentum = Vector3.zero;

	//Saved velocity from last frame;
	Vector3 savedVelocity = Vector3.zero;

	//Amount of downward gravitation;
	public float gravity = 30f;
	//Amount of downward gravitation when sliding down a slope;
	public float slideGravity = 30f;

	//Acceptable slope angle limit;
	public float slopeLimit = 80f;

	//time at which player left the ground
	private float lastGroundContact = 0.0f;

	//Enum describing basic controller states; 
	public enum ControllerState
	{
		Grounded,
		Sliding,
		Falling,
		Rising
	}
	ControllerState currentControllerState = ControllerState.Falling;
	 
	//Get references to all necessary components;
	void Awake () {
		mover = GetComponent<Mover>();
		tr = GetComponent<Transform>();
		jumpLeniencyInSeconds = jumpLeniency / 1000f;
		Setup();
	}

	//This function is called right after Awake(); It can be overridden by inheriting scripts;
	protected virtual void Setup()
	{
		
	}

	void Update()
	{
		//Reset flag;
		jumpKeyWasPressed = false;

		//Check if the jump key was pressed and set 'jumpKeyWasPressed';
		if(IsJumpKeyPressed())
		{
			jumpKeyWasPressed = true;
		}
	}

	//FixedUpdate;
	void FixedUpdate()
	{
		//Check if mover is grounded;
		mover.CheckForGround();

		mover.CheckIfHittingCeiling();

		//Determine controller state;
		HandleState();

		//Apply friction and gravity to 'momentum';
		HandleFrictionAndGravity();

		//Check if the player has initiated a jump;
		HandleJumping();

		//Calculate (normalized) movement direction;
		Vector3 _velocity = CalculateMovementDirection();

		//Save movement direction for later;
		Vector3 _velocityDirection = _velocity;

		//Multiply (normalized) velocity with movement speed;
		_velocity *= movementSpeed;

		//If player is in the air, multiply player velocity with 'airControl';
		if (currentControllerState == ControllerState.Rising || currentControllerState == ControllerState.Falling)
			_velocity = _velocityDirection * movementSpeed * airControl;

		//If player is standing (or walking) on a slope, decrease player velocity based on the slope's angle;
		if(currentControllerState == ControllerState.Sliding)
		{
			float _factor = Mathf.InverseLerp(90f, 0f, Vector3.Angle(tr.up, mover.GetGroundNormal()));
			_velocity *= _factor;  
		}

		//Add current momentum to velocity;
		_velocity += momentum;

		//If player is grounded or sliding on a slope, extend mover's sensor range;
		//This enables the player to walk up/down stairs and slopes without losing ground contact;
		mover.SetExtendSensorRange(currentControllerState == ControllerState.Grounded || currentControllerState == ControllerState.Sliding);

		//Set mover velocity;		
		mover.SetVelocity(_velocity);

		//Store velocity for next frame;
		savedVelocity = _velocity;
	}

	//Calculate and return movement direction based on player input;
	//This function can be overridden by inheriting scripts to implement different player controls;
	protected virtual Vector3 CalculateMovementDirection()
	{
		Vector3 _velocity = Vector3.zero;

		float _horizontalInput;
		float _verticalInput;

		//Get input;
		if(useRawInput){
			_horizontalInput = Input.GetAxisRaw(horizontalInputAxis);
			_verticalInput = Input.GetAxisRaw(verticalInputAxis);
		} else {
			_horizontalInput = Input.GetAxis(horizontalInputAxis);
			_verticalInput = Input.GetAxis(verticalInputAxis);
		}

		_velocity += tr.right * _horizontalInput;
		_velocity += tr.forward * _verticalInput;

		//Clamp movement vector to magnitude of 1f;
		if(_velocity.magnitude > 1f)
			_velocity.Normalize();

		return _velocity;
	}

	//Returns 'true' if the player presses the jump key;
	protected virtual bool IsJumpKeyPressed()
	{
		 return (Input.GetKey(jumpKey));
	}

	//Handle state transitions;
	//Determine current controller state based on 'savedVelocity' and whether the controller is grounded (or not);
	void HandleState()
	{
		//Check if vertical momentum is pointing upwards;
		bool _isRising = IsRisingOrFalling() && (VectorMath.GetDotProduct(momentum, tr.up) > 0f);
		//Check if controller is sliding;
		bool _isSliding = mover.IsGrounded() && (Vector3.Angle(mover.GetGroundNormal(), tr.up) > slopeLimit);

		switch(currentControllerState)
		{
		case ControllerState.Grounded:

			if(_isRising){
				currentControllerState = ControllerState.Rising;
				break;
			}
			if(!mover.IsGrounded()){
				currentControllerState = ControllerState.Falling;
				lastGroundContact = Time.time;
				OnGroundContactLost();
				break;
			}
			if(_isSliding){
				currentControllerState = ControllerState.Sliding;
				break;
			}
			break;

		case ControllerState.Falling:

			if(_isRising){
				currentControllerState = ControllerState.Rising;
				break;
			}
			if(mover.IsGrounded() && !_isSliding){
				currentControllerState = ControllerState.Grounded;
				OnGroundContactRegained(momentum);
				break;
			}
			if(_isSliding){
				currentControllerState = ControllerState.Sliding;
				OnGroundContactRegained(momentum);
				break;
			}
			break;

		case ControllerState.Sliding:

			if(_isRising){
				break;
			}
			if(!mover.IsGrounded()){
				currentControllerState = ControllerState.Falling;
				break;
			}
			if(mover.IsGrounded() && !_isSliding){
				OnGroundContactRegained(momentum);
				currentControllerState = ControllerState.Grounded;
				break;
			}
			break;

		case ControllerState.Rising:
			if(!_isRising){
				if(mover.IsGrounded() && !_isSliding){
					currentControllerState = ControllerState.Grounded;
					OnGroundContactRegained(momentum);
					break;
				}
				if(_isSliding){
					currentControllerState = ControllerState.Sliding;
					break;
				}
				if(!mover.IsGrounded()){
					currentControllerState = ControllerState.Falling;
					break;
				}
				if (mover.IsHittingCeiling()){
					currentControllerState = ControllerState.Falling;
					break;
                }
			}
			break;
		}
	}

	//Check if player has initiated a jump;
	void HandleJumping()
	{
		if(currentControllerState == ControllerState.Grounded || Time.time <= lastGroundContact + jumpLeniencyInSeconds)
		{
			//Check if jump key has been pressed;
			if(jumpKeyWasPressed)
			{
				//Reset flag;
				jumpKeyWasPressed = false;
				
				//Call events;
				OnGroundContactLost();
				OnJumpStart();

				currentControllerState = ControllerState.Rising;
			}
		}
	}

	//Apply friction to both vertical and horizontal momentum based on 'friction' and 'gravity';
	//Handle sliding down steep slopes;
	void HandleFrictionAndGravity()
	{
		Vector3 _verticalMomentum = Vector3.zero;
		Vector3 _horizontalMomentum = Vector3.zero;

		//Split momentum into vertical and horizontal components;
		if(momentum != Vector3.zero)
		{
			_verticalMomentum = VectorMath.ExtractDotVector(momentum, tr.up);
			_horizontalMomentum = momentum - _verticalMomentum;
		}

		//Add gravity to vertical momentum;
		if(currentControllerState == ControllerState.Sliding)
			_verticalMomentum -= tr.up * slideGravity * Time.deltaTime;
		else
			_verticalMomentum -= tr.up * gravity * Time.deltaTime;

		//Remove any downward force if the controller is grounded;
		if(currentControllerState == ControllerState.Grounded)
			_verticalMomentum = Vector3.zero;

        if (mover.IsHittingCeiling())
        {
			_verticalMomentum -= 0.1f * tr.up * jumpSpeed;
		}
		//Apply friction to horizontal momentum;
		_horizontalMomentum = VectorMath.IncrementVectorLengthTowardTargetLength(_horizontalMomentum, friction, Time.deltaTime, 0f);

		//Add horizontal and vertical momentum back together;
		momentum = _horizontalMomentum + _verticalMomentum;

		//Project the current momentum onto the current ground normal if the controller is sliding down a slope;
		if(currentControllerState == ControllerState.Sliding)
		{
			momentum = Vector3.ProjectOnPlane(momentum, mover.GetGroundNormal());
		}
	}

	//Events;

	//This function is called when the player has initiated a jump;
	void OnJumpStart()
	{
		//Add jump force to momentum;
		momentum += tr.up * jumpSpeed;

		//Call event;
		if(OnJump != null)
			OnJump(momentum);
	}

	//This function is called when the player has lost ground contact, i.e. is either falling or rising, or generally in the air;
	void OnGroundContactLost()
	{
		Vector3 _currentVelocity = savedVelocity;

		//Remove all vertical parts of '_currentVelocity';
		_currentVelocity = VectorMath.RemoveDotVector(_currentVelocity, tr.up);

		//Calculate length and direction from '_currentVelocity';
		float _length = _currentVelocity.magnitude;
		Vector3 _velocityDirection = Vector3.zero;

		//Calculate velocity direction;
		if(_length != 0f)
			_velocityDirection = _currentVelocity/_length;

		//Subtract from '_length', based on 'movementSpeed' and 'airControl', check for overshooting;
		if(_length >= movementSpeed * airControl)
			_length -= movementSpeed * airControl;
		else
			_length = 0f;

		momentum = _velocityDirection * _length;
	}

	//This function is called when the player has landed on a surface after being in the air;
	void OnGroundContactRegained(Vector3 _collisionVelocity)
	{
		//Call 'OnLand' event;
		if(OnLand != null)
			OnLand(_collisionVelocity);

		//Reset momentum;
		momentum = Vector3.zero;
	}

	//Helper functions;

	//Returns 'true' if vertical momentum is above a small threshold;
	private bool IsRisingOrFalling()
	{
		//Calculate current vertical momentum;
		Vector3 _verticalMomentum = VectorMath.ExtractDotVector(momentum, tr.up);

		//Setup threshold to check against;
		//For most applications, a value of '0.001f' is recommended;
		float _limit = 0.001f;

		//Return true if vertical momentum is above '_limit';
		return(_verticalMomentum.magnitude > _limit);
	}

	//Getters;

	//Get last frame's velocity;
	public Vector3 GetVelocity ()
	{
		return savedVelocity;
	}

	//Returns 'true' if controller is grounded (or sliding down a slope);
	public bool IsGrounded()
	{
		return(currentControllerState == ControllerState.Grounded || currentControllerState == ControllerState.Sliding);
	}

	//Events;
	public delegate void VectorEvent(Vector3 v);
	public event VectorEvent OnJump;
	public event VectorEvent OnLand;

}

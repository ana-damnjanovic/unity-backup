﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBodyManager : MonoBehaviour
{

    public GameObject regularBody;
    public GameObject bombBody;
    FixedJoint headJoint;

    void Start()
    {
        if (!regularBody)
        {
            FindRegularBody();
        }
        if (!bombBody)
        {
            FindBombBody();
        }
        headJoint = GetComponentInChildren<FixedJoint>();
        bombBody.SetActive(false);
    }

    public void EnableBombBody()
    {
        Destroy(regularBody);
        bombBody.transform.position = regularBody.transform.position;
        bombBody.transform.rotation = regularBody.transform.rotation;
        bombBody.SetActive(true);
        if (headJoint)
        {
            headJoint.connectedBody = bombBody.GetComponent<Rigidbody>();
        }
    }

    void FindRegularBody()
    {
        foreach (Transform child in transform)
        {
            if (child.name.Contains("BotBody"))
            {
                regularBody = child.gameObject;
            }
        }
    }

    void FindBombBody()
    {
        foreach (Transform child in transform)
        {
            if (child.name.Contains("BotBombBody"))
            {
                bombBody = child.gameObject;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VersusModeManager : MonoBehaviour
{
    public GameObject playerOne;
    public GameObject playerTwo;
    public Camera p1Camera;
    public Camera p2Camera;
    public GameObject topDownCamera;
    public float sceneReloadDelay = 1f;
    public static VersusModeManager Instance { get; private set; }

    void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
    }

    void Start()
    {
        playerOne = PlayerManager.Instance.playerOne;
        playerTwo = PlayerManager.Instance.playerTwo;
        p1Camera = PlayerManager.Instance.p1Camera;
        p2Camera = PlayerManager.Instance.p2Camera;
        if (!topDownCamera)
        {
            topDownCamera = GameObject.Find("TopDownCamera");
        }
    }

    public void OnPlayerDeath(GameObject deadPlayer)
    {
        if (topDownCamera.activeSelf)
        {
            ResetScene();
        }
        else
        {
            if (deadPlayer == playerOne)
            {
                StartCoroutine(ToggleCamerasAndReset(p2Camera, p1Camera));
            }
            else
            {
                StartCoroutine(ToggleCamerasAndReset(p1Camera, p2Camera));
            }   
        }
    }

    IEnumerator ToggleCamerasAndReset(Camera aliveCamera, Camera deadCamera)
    {
        deadCamera.rect = new Rect(0f, 0.5f, 0f, 0f);
        aliveCamera.rect = new Rect(0f, 0f, 1f, 1f);
        yield return new WaitForSeconds(sceneReloadDelay);
        ResetScene();
    }

    void ResetScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }
}

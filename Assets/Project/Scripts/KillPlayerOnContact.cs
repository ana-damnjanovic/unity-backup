﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillPlayerOnContact : MonoBehaviour
{
    public LayerMask playerLayers;

    void OnCollisionEnter(Collision other)
    {
        if (playerLayers == (playerLayers | (1 << other.gameObject.layer)))
        {
            KillPlayer(other.transform.root);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (playerLayers == (playerLayers | (1 << other.gameObject.layer)))
        {
            KillPlayer(other.transform.root);
        }
    }

    void KillPlayer(Transform player)
    {
        GameOver gameOver = player.GetComponentInChildren<GameOver>();
        if (gameOver)
        {
            gameOver.KillPlayer();
        }
    }
}

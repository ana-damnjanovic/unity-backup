﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class LookAtTarget : MonoBehaviour
{
    Transform target;
    ConstraintSource source;
    void Start()
    {
        target = this.transform.parent.GetComponentInChildren<Pathfinding.AIDestinationSetter>().target;
        LookAtConstraint lookAtConstraint = this.GetComponent<LookAtConstraint>();
        if (target)
        {
            source.sourceTransform = target.transform;
            source.weight = 1;
            lookAtConstraint.AddSource(source);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHoleBehaviour : MonoBehaviour
{
    public float expandTime = 5f;
    public float shrinkTime = 1f;
    public float minSize = 0.15f;
    public float maxSize = 0.5f;
    public float repelForce = 500f;
    
    Vector3 min;
    Vector3 max;
    Vector3 startSize;
    float startTime;
    Magnet magnet;
    float magnetMinSize;
    float magnetMaxSize;
    RayFire.RayfireActivator activator;

    void Start()
    {
        activator = GetComponentInChildren<RayFire.RayfireActivator>();
        min = new Vector3(minSize, minSize, minSize);
        max = new Vector3(maxSize, maxSize, maxSize);
        magnet = GetComponent<Magnet>();
        magnetMinSize = magnet.outerRadius;
        startSize = transform.localScale;
        magnetMaxSize = magnet.outerRadius * (maxSize / startSize.x);
        startTime = Time.time;
        StartCoroutine(GrowAndShrink());
    }

    void Update()
    {
    }

    IEnumerator GrowAndShrink()
    {
        yield return StartCoroutine(Grow(expandTime));
        yield return new WaitForSeconds(0.5f);
        yield return StartCoroutine(Shrink(shrinkTime));

        Destroy(this.gameObject);
    }

    IEnumerator Grow(float time)
    {
        float elapsedTime = 0;
        while (elapsedTime < time)
        {
            transform.localScale = Vector3.Lerp(startSize, max, (elapsedTime / time));
            magnet.outerRadius = Mathf.Lerp(magnetMinSize, magnetMaxSize, (elapsedTime / time));
            elapsedTime += Time.deltaTime;

            yield return null;
        }
    }

    IEnumerator Shrink(float time)
    {
        float elapsedTime = 0;
        while (elapsedTime < time)
        {
            transform.localScale = Vector3.Lerp(max, min, (elapsedTime / time));
            magnet.outerRadius = Mathf.Lerp(magnetMaxSize, magnetMinSize, (elapsedTime / time));
            elapsedTime += Time.deltaTime;

            yield return null;
        }
        magnet.magnetForce = repelForce;
        magnet.attract = false;
        yield return new WaitForSeconds(0.1f);
    }

    void SetGlobalScale(Transform t, Vector3 globalScale)
    {
        t.localScale = Vector3.one;
        t.localScale = new Vector3(globalScale.x / transform.lossyScale.x, globalScale.y / transform.lossyScale.y, globalScale.z / transform.lossyScale.z);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBouncedBlade : MonoBehaviour
{
    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Blade")
        {
            Transform blade = coll.transform.root.Find("RotatingStickyBlade");
            Vector3 direction = Vector3.Reflect(blade.position, coll.contacts[0].normal);
            blade.rotation = Quaternion.LookRotation(direction);
            blade.root.rotation = Quaternion.identity;
        }
    }
}

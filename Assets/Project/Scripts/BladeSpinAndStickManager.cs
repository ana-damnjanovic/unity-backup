﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeSpinAndStickManager : MonoBehaviour
{
    Rigidbody rb;
    Aura2API.AutoMoveAndRotate rotator;
    FixedJoint joint;
    StickyStickStuck.SSS sss;
    GameObject rayfireBladeChild;
    void Start()
    {
        rb = this.GetComponent<Rigidbody>();
        rotator = this.GetComponent<Aura2API.AutoMoveAndRotate>();
        sss = this.GetComponent<StickyStickStuck.SSS>();
        sss.enabled = false;
        rayfireBladeChild = transform.Find("RayfireBlade").gameObject;
    }

    void OnTriggerEnter(Collider coll)
    {
        sss.enabled = true;
        GameObject target = coll.gameObject;
        if (target.tag != "Player" && target.tag != "PickUp")
        {
            if (target.tag != "Destructible" || (target.tag == "Destructible" && target.name.Contains("BotBomb")))
            {
                rotator.enabled = false;
                rb.constraints = RigidbodyConstraints.FreezeAll;
                if (!GetComponent<FixedJoint>())
                {
                    joint = this.gameObject.AddComponent<FixedJoint>();
                    joint.breakForce = Mathf.Infinity;
                }
                joint.connectedBody = coll.gameObject.GetComponent<Rigidbody>();
                if (rayfireBladeChild)
                {
                    Destroy(rayfireBladeChild.GetComponent<RayFire.RayfireBlade>());
                }
                rb.constraints = RigidbodyConstraints.None;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VortexDemolish : MonoBehaviour
{
    void OnCollisionEnter(Collision other)
    {
        if (other.transform.root.name.Contains("Vortex"))
        {
            GetComponent<RayFire.RayfireRigid>().Demolish();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.root.name.Contains("Vortex"))
        {
            GetComponent<RayFire.RayfireRigid>().Demolish();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleTopDownMode : MonoBehaviour
{
    public KeyCode toggleKey = KeyCode.T;
    public GameObject topDownCamera;
    public GameObject playerOne;
    public GameObject playerTwo;    
    public Transform playerOneBody;
    public Transform playerTwoBody;
    public GameObject bladeCatcherWalls;
    Quaternion p1StartRotation;
    Quaternion p2StartRotation;
    Vector3 p1StartForward;
    Vector3 p2StartForward;
    bool p1RotationAdjusted;
    bool p2RotationAdjusted;
    GameObject p1TopDownItem;
    GameObject p2TopDownItem;
    CameraController p1CameraController;
    CameraController p2CameraController;
    CameraWalkerController p1cameraWalkerController;
    CameraWalkerController p2cameraWalkerController;
    float jumpSpeed;
    MSDragObjects p1DragObjects;
    MSDragObjects p2DragObjects;
    AimingUIManager p1AimingUI;
    AimingUIManager p2AimingUI;
    BladeSpawner p1BladeSpawner;
    BladeSpawner p2BladeSpawner;
    MouseCursorLock mouseLock;
    bool toggledThisFrame;

    void Start()
    {
        playerOne = PlayerManager.Instance.playerOne;
        playerTwo = PlayerManager.Instance.playerTwo;
        playerOneBody = playerOne.transform.Find("ModelRoot").Find("CameraControlsFirst");
        playerTwoBody = playerTwo.transform.Find("ModelRoot").Find("CameraControlsFirst");
        p1StartRotation = playerOneBody.rotation;
        p2StartRotation = playerTwoBody.rotation;
        p1StartForward = playerOneBody.forward;
        p2StartForward = playerTwoBody.forward;
        p1RotationAdjusted = false;
        p2RotationAdjusted = false;
        p1TopDownItem = PlayerManager.Instance.p1Camera.gameObject.transform.Find("TopDownHeldItemLocation").gameObject;
        p2TopDownItem = PlayerManager.Instance.p2Camera.gameObject.transform.Find("TopDownHeldItemLocation").gameObject;
        p1CameraController = playerOne.GetComponentInChildren<CameraController>();
        p2CameraController = playerTwo.GetComponentInChildren<CameraController>();
        p1cameraWalkerController = playerOne.GetComponentInChildren<CameraWalkerController>();
        p2cameraWalkerController = playerTwo.GetComponentInChildren<CameraWalkerController>();
        jumpSpeed = p1cameraWalkerController.jumpSpeed;
        p1DragObjects = PlayerManager.Instance.p1Camera.gameObject.GetComponent<MSDragObjects>();
        p2DragObjects = PlayerManager.Instance.p2Camera.gameObject.GetComponent<MSDragObjects>();
        p1AimingUI = PlayerManager.Instance.p1Camera.gameObject.GetComponent<AimingUIManager>();
        p2AimingUI = PlayerManager.Instance.p2Camera.gameObject.GetComponent<AimingUIManager>();
        p1BladeSpawner = PlayerManager.Instance.p1Camera.gameObject.GetComponent<BladeSpawner>();
        p2BladeSpawner = PlayerManager.Instance.p2Camera.gameObject.GetComponent<BladeSpawner>();
        mouseLock = GetComponent<MouseCursorLock>();
        toggledThisFrame = true;
        if (!topDownCamera)
        {
            topDownCamera = GameObject.Find("TopDownCamera");
        }
        //if (!bladeCatcherWalls)
        //{
        //    bladeCatcherWalls = GameObject.Find("BladeCatcherWalls");
        //}
    }

    void Update()
    {
        if (Input.GetKeyDown(toggleKey))
        {
            topDownCamera.SetActive(!topDownCamera.activeSelf);
            toggledThisFrame = true;
        }
        if (topDownCamera.activeSelf && (toggledThisFrame || !p2RotationAdjusted))
        {
            mouseLock.topDownMode = true;
            if (!p1RotationAdjusted)
            {
                playerOneBody.rotation = p1StartRotation;
                playerOneBody.forward = p1StartForward;
                p1CameraController.SetFacingDirection(p1StartForward);
                p1RotationAdjusted = true;
            }
            p1DragObjects.enabled = false;
            p1TopDownItem.SetActive(true);
            p1AimingUI.enabled = true;
            p1CameraController.freezeCameraRotation = true;
            p1cameraWalkerController.jumpSpeed = 0f;
            p1BladeSpawner.topDownMode = true;

            if (!p2RotationAdjusted && playerTwo.activeSelf)
            {
                playerTwoBody.rotation = p2StartRotation;
                playerTwoBody.forward = p2StartForward;
                p2CameraController.SetFacingDirection(p2StartForward);
                p2RotationAdjusted = true;
            }
            p2DragObjects.enabled = false;
            p2TopDownItem.SetActive(true);
            p2AimingUI.enabled = true;
            p2CameraController.freezeCameraRotation = true;
            p2cameraWalkerController.jumpSpeed = 0f;
            p2BladeSpawner.topDownMode = true;
            //if (bladeCatcherWalls)
            //{
            //    bladeCatcherWalls.SetActive(true);
            //}
            toggledThisFrame = false;
        }
        if (!topDownCamera.activeSelf && toggledThisFrame)
        {
            mouseLock.topDownMode = false;
            p1RotationAdjusted = false;
            p1DragObjects.enabled = true;
            p1TopDownItem.SetActive(false);
            p1AimingUI.enabled = false;
            p1CameraController.freezeCameraRotation = false;
            p1cameraWalkerController.jumpSpeed = jumpSpeed;
            p1BladeSpawner.topDownMode = false;

            p2RotationAdjusted = false;
            p2DragObjects.enabled = true;
            p2TopDownItem.SetActive(false);
            p2AimingUI.enabled = false;
            p2CameraController.freezeCameraRotation = false;
            p2cameraWalkerController.jumpSpeed = jumpSpeed;
            p2BladeSpawner.topDownMode = false;
            //if (bladeCatcherWalls)
            //{
            //    bladeCatcherWalls.SetActive(false);
            //}
            toggledThisFrame = false;
        }
    }
}

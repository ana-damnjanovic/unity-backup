﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Level
{
    [MinMaxSlider(0f, 100f)]
    public MinMax bombBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax shieldBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax sniperBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax invisibleBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax duplicatorBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax blackHoleBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax supportBots;
    [MinMaxSlider(0f, 100f)]
    public MinMax advancedSniperBots;
}

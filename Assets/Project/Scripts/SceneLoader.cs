﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void Load(string sceneName, float loadingDelay)
    {
        StartCoroutine(DelayLoading(sceneName, loadingDelay));
    }

    IEnumerator DelayLoading(string name, float delay)
    {
        yield return new WaitForSeconds(delay);
        SceneManager.LoadScene(name, LoadSceneMode.Single);
    }
}

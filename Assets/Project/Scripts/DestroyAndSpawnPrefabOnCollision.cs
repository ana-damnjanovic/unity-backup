﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAndSpawnPrefabOnCollision : MonoBehaviour
{
    public GameObject prefabToSpawn;
    public LayerMask layerMask;

    void OnCollisionEnter(Collision other)
    {
        if (layerMask == (layerMask | (1 << other.gameObject.layer)))
        {
            SpawnEffectAndDestroy();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (layerMask == (layerMask | (1 << other.gameObject.layer)))
        {
            SpawnEffectAndDestroy();
        }
    }

    void SpawnEffectAndDestroy()
    {
        if (prefabToSpawn)
        {
            Instantiate(prefabToSpawn, transform.position, transform.rotation);
        }
        Destroy(this.gameObject);
    }
}

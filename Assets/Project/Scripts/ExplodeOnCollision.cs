﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeOnCollision : MonoBehaviour
{
    public float explosionPower = 7.0f;
    public float explosionRadius = 2.5f;
    public GameObject bigExplosionPrefab;
    public LayerMask layerMask = ~0;
    public bool modifyTerrain = false;
    public float diggerSize = 5f;
    RayFire.RayfireBomb rayfireBomb;
    void Start()
    {
        rayfireBomb = GetComponent<RayFire.RayfireBomb>();
    }

    void OnCollisionEnter(Collision other)
    {
        if (GetComponent<AcR_Body>() && other.gameObject.layer == LayerMask.NameToLayer("Blade")) // specific logic for 2 legged guy enemy
        {
            GetComponent<AcR_Body>().enabled = false;
            GetComponent<WalkTowardsTarget>().enabled = false;
        }
        else if (layerMask == (layerMask | (1 << other.gameObject.layer)))
        {
            Explosive explosive = this.gameObject.AddComponent(typeof(Explosive)) as Explosive;
            explosive.power = explosionPower;
            explosive.radius = explosionRadius;
            explosive.explosionPrefab = bigExplosionPrefab;
            rayfireBomb.Explode(0.0f);
            explosive.explode();
            if (modifyTerrain && GetComponent<Digger.DiggerMasterRuntime>())
            {
                GetComponent<Digger.DiggerMasterRuntime>().Modify(this.transform.position, Digger.BrushType.Sphere, Digger.ActionType.Dig, 0, 4f, diggerSize, true, true);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoopBlade : MonoBehaviour
{
    public Transform oppositeWall;

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Blade")
        {
            Vector3 reflection = Vector3.Reflect(coll.transform.root.position, Vector3.forward);
            Vector3 oppositeWallWorldPt = oppositeWall.TransformPoint(oppositeWall.position);
            coll.transform.root.position = new Vector3(coll.transform.root.position.x, coll.transform.root.position.y, oppositeWall.position.z);
        }
    }
}

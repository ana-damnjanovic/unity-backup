﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeSpawner : MonoBehaviour
{
    public GameObject bladePrefab;
    public GameObject bladeThrowEffect;
    public KeyCode windowsSpawnKey = KeyCode.Mouse0;
    public KeyCode macSpawnKey = KeyCode.JoystickButton11;
    public int maxNumberOfBlades = 1;
    public bool useMacControls = false;
    public bool topDownMode = false;
    public bool reuseSingleBlade = false;
    [Tooltip("The default distance of the target in case raycasting fails")]
    public int defaultDistance = 25;
    public LayerMask layerMask;
    KeyCode spawnKey;
    bool bladeSpawnedThisUpdate;
    int numBlades;
    GameObject blade;
    bool canSpawn;
    Transform topDownItemLocation;
    Transform topDownBladeSpawn;
    ParticleSystem gunEffect;
    Transform weapon;
    WeaponManager weaponManager;

    void Start()
    {
        bladeSpawnedThisUpdate = false;
        canSpawn = true;
        numBlades = 0;
        topDownBladeSpawn = transform.Find("BladeSpawn");
        weapon = transform.Find("Weapon");
        if (weapon && weapon.GetComponent<WeaponManager>())
        {
            weaponManager = weapon.GetComponent<WeaponManager>();
            maxNumberOfBlades = weaponManager.GetAmmoMax();
        }
        if (weapon && weapon.Find("Gun"))
        {
            gunEffect = weapon.Find("Gun").GetComponentInChildren<ParticleSystem>();
        }
        if (useMacControls)
        {
            spawnKey = macSpawnKey;
        }
        else
        {
            spawnKey = windowsSpawnKey;
        }
    }

    void Update()
    {
        if (weaponManager)
        {
            numBlades = weaponManager.GetNumAmmoShot();
            maxNumberOfBlades = weaponManager.GetAmmoMax();
        }
        if (Input.GetKeyDown(spawnKey))
        {
            if (canSpawn && !bladeSpawnedThisUpdate)
            {
                bladeSpawnedThisUpdate = true;
                if (reuseSingleBlade)
                {
                    if (blade)
                    {
                        Destroy(blade);
                    }
                    blade = Instantiate(bladePrefab, this.transform.position, this.transform.rotation);
                }
                else if (numBlades < maxNumberOfBlades)
                {
                    numBlades++;
                    if (topDownMode)
                    {
                        blade = Instantiate(bladePrefab, topDownBladeSpawn.transform.position, topDownBladeSpawn.transform.rotation);
                        Instantiate(bladeThrowEffect, topDownBladeSpawn.transform.position, topDownBladeSpawn.transform.rotation);
                    }
                    else
                    {
                        GameObject ammo;
                        if (weaponManager)
                        {
                            ammo = weaponManager.GetActiveAmmo();
                            weaponManager.IncrementNumAmmo();
                        }
                        else
                        {
                            ammo = bladePrefab;
                        }
                        Vector3 spawnPosition;
                        if (weaponManager)
                        {
                            if (weaponManager.GetActiveWeapon().name == "Gun" && gunEffect)
                            {
                                spawnPosition = gunEffect.transform.position;
                            }
                            else
                            {
                                spawnPosition = weaponManager.GetActiveWeapon().transform.Find("MuzzleFlash").position;
                            }
                        }
                        else
                        {
                            spawnPosition = this.transform.position;
                        }
                        blade = Instantiate(ammo, spawnPosition, this.transform.rotation);
                        if (weaponManager)
                        {
                            Vector3 target = CalculateTarget();
                            Debug.DrawRay(spawnPosition, target - spawnPosition, Color.white, Vector3.Distance(target, spawnPosition), true);
                            blade.GetComponent<BladeBehaviour>().UpdateForward(target);
                        }
                        if (gunEffect)
                        {
                            gunEffect.Play();
                        }
                    }
                }
                blade.GetComponent<BladeBehaviour>().spawner = this;
            }
        }
        if(Input.GetKeyUp(spawnKey))
        {
            bladeSpawnedThisUpdate = false;
        }
    }

    Vector3 CalculateTarget()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            if (hit.transform.gameObject.tag != "Blade")
            {
                //Debug.DrawRay(ray.origin, hit.point - ray.origin, Color.green, Vector3.Distance(ray.origin, hit.point), true);
                return hit.point;
            }
        }
        return Camera.main.ScreenToWorldPoint(new Vector3(Screen.width / 2, Screen.height / 2, defaultDistance)); ;
    }

    public void DecreaseNumBlades()
    {
        if (weaponManager)
        {
            weaponManager.DecrementNumAmmo();
        }
        else
        {
            if (numBlades >= 1)
            {
                numBlades--;
            }
        }
    }

    public bool GetCanSpawn()
    {
        return canSpawn;
    }

    public void SetCanSpawn(bool val)
    {
        canSpawn = val;
    }

    public int GetNumBladesSpawned()
    {
        return numBlades;
    }

    public int GetNumBladesRemaining()
    {
        return maxNumberOfBlades - numBlades;
    }

    public void LoseRemainingBlades()
    {
        numBlades = maxNumberOfBlades;
    }
}

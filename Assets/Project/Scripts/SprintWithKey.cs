﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprintWithKey : MonoBehaviour
{
    public float sprintMultiplier = 1.75f;
    public KeyCode sprintKey = KeyCode.LeftShift;
    bool isSprinting;
    CameraWalkerController walker;
    float origSpeed;
    float sprintSpeed;
    float origAirControl;
    float sprintAirControl;

    void Start()
    {
        walker = GetComponentInChildren<CameraWalkerController>();
        origSpeed = walker.movementSpeed;
        origAirControl = walker.airControl;
        sprintSpeed = walker.movementSpeed * sprintMultiplier;
        sprintAirControl = 1 / sprintMultiplier;
    }

    void Update()
    {
        if (Input.GetKey(sprintKey))
        {
            walker.movementSpeed = sprintSpeed;
            walker.airControl = sprintAirControl;
        }
        if (Input.GetKeyUp(sprintKey))
        {
            StartCoroutine(StopSprinting());
        }
    }

    IEnumerator StopSprinting()
    {
        walker.movementSpeed = origSpeed * sprintMultiplier * 0.9f;
        walker.airControl = origAirControl;
        yield return new WaitForSeconds(0.25f);
        walker.movementSpeed = origSpeed;
    }
}

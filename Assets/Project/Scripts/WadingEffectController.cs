﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WadingEffectController : MonoBehaviour
{
    [Tooltip("Enable particle effect when standing on objects in this Layer")]
    public string _wadingLayer = "Water";
    public bool useSphereCast = false;
    ParticleSystem wadingEffect;
    bool isOnLiquid;
    bool isPlaying;
    RaycastHit hit;
    int liquidLayer;
    Vector3 offset;

    void Start()
    {
        wadingEffect = this.GetComponentInChildren<ParticleSystem>();
        isPlaying = false;
        isOnLiquid = false;
        liquidLayer = LayerMask.NameToLayer(_wadingLayer);
        offset = new Vector3(0, 1, 0);
    }

    void Update()
    {
        isOnLiquid = CheckIfOnLiquid();
        if (isOnLiquid && !isPlaying)
        {
            isPlaying = true;
            wadingEffect.Play();
        }
        if (!isOnLiquid)
        {
            wadingEffect.Stop();
            isPlaying = false;
        }
    }

    bool CheckIfOnLiquid()
    {
        if (useSphereCast)
        {
            return (Physics.SphereCast(transform.position + offset, 1, Vector3.down, out hit) && hit.transform.gameObject.layer == liquidLayer);
        }
        else
        {
            return (Physics.Raycast(this.transform.position, Vector3.down, out hit, 2) && hit.transform.gameObject.layer == liquidLayer);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sprint : MonoBehaviour
{

    public float sprintMultiplier = 1.75f;
    public float sprintDuration = 5.0f;
    bool isSprinting;
    CameraWalkerController walker;
    float origSpeed;
    float sprintSpeed;

    void Start()
    {
        walker = GetComponentInChildren<CameraWalkerController>();
        origSpeed = walker.movementSpeed;
        sprintSpeed = walker.movementSpeed * sprintMultiplier;
    }

    public void StartSprinting()
    {
        StartCoroutine(SprintForSeconds(sprintDuration));
    }

    IEnumerator SprintForSeconds(float duration)
    {
        walker.movementSpeed = sprintSpeed;
        yield return new WaitForSeconds(duration);
        walker.movementSpeed = sprintSpeed * 0.5f;
        yield return new WaitForSeconds(0.5f);
        walker.movementSpeed = origSpeed;
    }
}

﻿using UnityEngine;
using System.Collections;

public class PlayerMouseAiming : MonoBehaviour
{
    public string xAxis = "Mouse X";
    public string yAxis = "Mouse Y";
    public Camera topDownCamera;
    public LayerMask layerMask;
    public Transform topDownBladeSpawn;
    Transform targettingCube;
    RaycastHit hit;    
    // from https://answers.unity.com/questions/631897/right-stick-aiming-problem-dual-stick-shooter.html
    // and https://answers.unity.com/questions/932115/top-down-aim-at-mouse.html

    void Start()
    {
        if (!topDownBladeSpawn)
        {
            topDownBladeSpawn = transform.parent.Find("BladeSpawn");
        }
        targettingCube = transform.Find("TargettingCube");
    }
    void Update()
    {
        Vector3 direction = GetGroundPosition(this.transform.position);
        direction.y = 0f;
        transform.LookAt(direction);
        targettingCube.position = direction;
        Vector3 bladeDirection = new Vector3(direction.x, topDownBladeSpawn.transform.position.y, direction.z);
        topDownBladeSpawn.LookAt(bladeDirection);
    }

    Vector3 GetGroundPosition(Vector3 defaultValue)
    {
        var ray = topDownCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 1f));

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100f, layerMask, QueryTriggerInteraction.Ignore))
        {
            return hit.point;
        }

        return defaultValue;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HovercraftBehaviour : MonoBehaviour
{
    Rigidbody rb;
    bool containsPlayer;
    void Start()
    {
        rb = GetComponentInChildren<Rigidbody>();
        rb.isKinematic = true;
        containsPlayer = false;
    }

    void Update()
    {
        if (containsPlayer)
        {
            rb.isKinematic = false;
        }
        else
        {
            rb.isKinematic = true;
        }
    }

    public void ParentPlayer(GameObject player)
    {
        player.transform.root.parent = this.transform;
        containsPlayer = true;
    }

    public void UnparentPlayer(GameObject player)
    {
        player.transform.parent = null;
        containsPlayer = false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectPickup : MonoBehaviour
{
    public float massToBulletMultiplier = 5;
    BladeSpawner bladeSpawner;
    GameOver gameOver;
    GameObject ground;
    Sprint sprint;

    void Start()
    {
        bladeSpawner = GetComponentInChildren<BladeSpawner>();
        gameOver = GetComponentInChildren<GameOver>();
        sprint = GetComponentInChildren<Sprint>();
    }
    void OnTriggerEnter(Collider collider)
    {
        GameObject obj = collider.gameObject;
        if (obj.tag == "Blade")
        {
            BladeBehaviour bladeBehaviour = obj.transform.root.GetComponent<BladeBehaviour>();
            int numCollisions = bladeBehaviour.GetNumCollisionsWithPlayer();
            if (numCollisions > 2)
            {
                bladeSpawner.DecreaseNumBlades();
                bladeBehaviour.PickUp();
            }
            else
            {
                bladeBehaviour.IncrementNumCollisionsWithPlayer();
            }
        }
    }

    void OnTriggerStay(Collider collider)
    {
        GameObject obj = collider.gameObject;
        if (obj.tag == "PickUp")
        {
            if (obj.name.Contains("ExtraBlade"))
            {
                bladeSpawner.maxNumberOfBlades++;
            }
            else if (obj.name.Contains("Invincibility"))
            {
                gameOver.StartInvincibilityTimer(5.0f);
            }
            else if (obj.name.Contains("Sprint"))
            {
                sprint.StartSprinting();
            }
            Destroy(obj);
        }
    }
}

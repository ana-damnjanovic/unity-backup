﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanOnStart : MonoBehaviour
{
    AstarPath path;
    void Start()
    {
        path = GetComponent<AstarPath>();
        path.Scan();
    }
}

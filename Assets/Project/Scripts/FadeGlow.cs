﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeGlow : MonoBehaviour
{
    Renderer glowRenderer;
    Material material;
    float t = 0.0f;
    public float duration = 1.0f;
    public float startIntensity = 0.603931f;
    public float finalIntensity = -1.0f;
    public Material botInterior;
    Color baseColor;
    void Start()
    {
        glowRenderer = GetComponent<Renderer>();
        if (botInterior)
        {
            Material[] mats = { glowRenderer.material, botInterior };
            glowRenderer.materials = mats; 
        }
        material = glowRenderer.materials[1];
        material.EnableKeyword("_EMISSION");
        baseColor = material.GetColor("_EmissionColor");
    }

    void Update()
    {
        float emission = Mathf.Lerp(startIntensity, finalIntensity, t);
        Color currentColor = baseColor * Mathf.LinearToGammaSpace(emission);
        material.SetColor("_EmissionColor", currentColor);
        t += Time.deltaTime / duration;
    }
}

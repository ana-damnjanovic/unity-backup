﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour
{
    public float killHeight = -0.5f;
    public float invincibilityTime = 3.0f;
    public GameObject splashPrefab;
    public GameObject respawnPoint;
    public GameObject invincibilityEffect;
    public GameObject deathExplosionEffect;
    public float respawnYOffset = 5.0f;
    public bool versusMode = false;
    Rigidbody rb;
    bool isDead;
    bool canRevive;
    bool isInvincible;
    bool isHidden;
    DeathUIManager deathManager;
    VersusModeManager versusModeManager;
    Vector3 spawnPosition;
    Quaternion spawnRotation;
    BladeSpawner bladeSpawner;
    GameObject targettingCube;
    RayFire.RayfireRigid rfRigid;
    void Start()
    {
        isDead = false;
        isInvincible = false;
        canRevive = false;
        isHidden = false;
        rb = this.gameObject.GetComponent<Rigidbody>();
        spawnPosition = this.transform.position;
        spawnRotation = this.transform.rotation;
        bladeSpawner = GetComponentInChildren<BladeSpawner>();
        targettingCube = bladeSpawner.transform.Find("TopDownHeldItemLocation").Find("TargettingCube").gameObject;
        rfRigid = GetComponentInChildren<RayFire.RayfireRigid>();
        if (!invincibilityEffect)
        {
            if (transform.Find("InvincibilityEffect"))
            {
                invincibilityEffect = transform.Find("InvincibilityEffect").gameObject;
                invincibilityEffect.SetActive(false);
            }
            else
            {
                invincibilityEffect = null;
            }
        }
        if (versusMode)
        {
            deathManager = null;
            versusModeManager = VersusModeManager.Instance;
        }
        else
        {
            if (GameObject.Find("DeathUIManager"))
            {
                deathManager = GameObject.Find("DeathUIManager").GetComponent<DeathUIManager>();
            }
            versusModeManager = null;
        }
    }

    void Update()
    {

        if (!isDead && this.transform.position.y < killHeight && !isInvincible)
        {
            if (splashPrefab)
            {
                GameObject splash = Instantiate(splashPrefab, this.transform.position, this.transform.rotation);
                splash.transform.parent = transform;
            }
            if (respawnPoint)
            {
                Respawn();
            }
            else
            {
                Die();
            }

        }
        if (isDead && !canRevive && !isHidden)
        {
            if (rfRigid && deathExplosionEffect)
            {
                Instantiate(deathExplosionEffect, transform.position, deathExplosionEffect.transform.rotation);
                rfRigid.Activate();
                rfRigid.Demolish();
            }
            DisableMeshRenderers();
            DisableColliders();
            DisableLineRenderer();
            targettingCube.SetActive(false);
            isHidden = true;
        }
        if (isInvincible)
        {
            if (this.transform.position.y != spawnPosition.y)
            {
                this.transform.position = new Vector3(transform.position.x, spawnPosition.y, transform.position.z);
                rb.constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.tag == "Blade")
        {
            if (!versusMode && isDead && canRevive)
            {
                Destroy(collider.gameObject);
                Revive();
            }
            else if (versusMode && collider.transform.root.GetComponent<BladeBehaviour>().spawner.transform.root != this.transform.root)
            {
                Die();
            }
        }
        if ((collider.gameObject.name.Contains("SniperBomb") || collider.gameObject.name.Contains("BlackHoleInterior")) && !isInvincible)
        {
            KillPlayer();
        }
    }

    void Die()
    {
        isDead = true;
        if (this.transform.position.y != killHeight)
        {
            this.transform.position = new Vector3(transform.position.x, killHeight, transform.position.z);
        }
        rb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
        rb.isKinematic = true;
        if (!versusMode && deathManager)
        {
            deathManager.OnPlayerDeath();
        }
        else
        {
            canRevive = false;
            versusModeManager.OnPlayerDeath(this.gameObject);
        }
    }

    void Revive()
    {
        isDead = false;
        this.transform.position = spawnPosition;
        rb.isKinematic = false;
        rb.collisionDetectionMode = CollisionDetectionMode.Continuous;
        canRevive = false;
        bladeSpawner.LoseRemainingBlades();
        StartCoroutine(InvincibilityTimer(invincibilityTime));
    }

    void Respawn()
    {
        isDead = false;
        this.transform.position = respawnPoint.transform.position + new Vector3(0f, respawnYOffset, 0f);
        canRevive = false;
    }

    void DisableMeshRenderers()
    {
        Component[] meshRenderers = this.GetComponentsInChildren<MeshRenderer>();
        foreach (MeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.enabled = false;
        }
    }

    void DisableLineRenderer()
    {
        LineRenderer lineRenderer = this.GetComponentInChildren<LineRenderer>();
        if (lineRenderer)
        {
            lineRenderer.enabled = false;
        }
    }

    void DisableColliders()
    {
        Collider[] colliders = this.GetComponentsInChildren<Collider>();
        foreach (Collider coll in colliders)
        {
            coll.enabled = false;
        }
    }

    IEnumerator InvincibilityTimer(float time)
    {
        isInvincible = true;
        invincibilityEffect.SetActive(true);
        yield return new WaitForSeconds(time);
        isInvincible = false;
        invincibilityEffect.SetActive(false);
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
    }

    public void StartInvincibilityTimer(float time)
    {
        StartCoroutine(InvincibilityTimer(time));
    }

    public void SetCanRevive(bool value)
    {
        canRevive = value;
    }

    public bool CanBeRevived()
    {
        return canRevive;
    }

    public bool IsPlayerDead()
    {
        return isDead;
    }

    public void KillPlayer() //instantly kill player, no revives
    {
        if (respawnPoint)
        {
            Respawn();
        }
        else
        {
            Die();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizePlacement : MonoBehaviour
{
    public List<GameObject> objects;
    [Tooltip("If transforms are specified, the objects will be randomized to those positions and rotations." +
        " Otherwise, the script will attempt to generate positions within the specified min/max values.")]
    public List<Transform> transforms;
    public MapMagic.Core.MapMagicObject mapMagic;
    public GameObject draftTerrain;
    public float minX = -65f;
    public float maxX = 45f;
    public float minZ = -80f;
    public float maxZ = 80f;
    public float defaultHeight = 35f;
    [Tooltip("Prints each object's position to the console")]
    public bool logPositions = false;
    bool generated = false;
    List<Vector3> positions = new List<Vector3>();
    void Start()
    {
        if (!draftTerrain)
        {
            draftTerrain = null;
        }
        if (transforms.Count == 0 && mapMagic)
        {
            if ((draftTerrain && draftTerrain.activeSelf) || !mapMagic.IsGenerating())
            {
                GeneratePositions();
                generated = true;
                RandomizeFromPositions();
            }
        }
        else
        {
            RandomizeFromTransforms();
        }
    }

    void Update()
    {
        if (!generated && transforms.Count == 0 && mapMagic)
        {
            if ((draftTerrain && draftTerrain.activeSelf) || !mapMagic.IsGenerating())
            {
                GeneratePositions();
                generated = true;
                RandomizeFromPositions();
            }
        }
    }

    void RandomizeFromTransforms()
    {
        for (int i = 0; i < transforms.Count; i++)
        {
            if (objects.Count != 0)
            {
                int rand = Random.Range(0, objects.Count - 1);
                objects[rand].transform.position = new Vector3(transforms[i].position.x, objects[rand].transform.position.y, transforms[i].position.z);
                objects[rand].transform.rotation = transforms[i].rotation;
                objects.Remove(objects[rand]);
            }
        }
    }

    void RandomizeFromPositions()
    {
        for (int i = 0; i < positions.Count; i++)
        {
            if (objects.Count != 0)
            {
                int rand = Random.Range(0, objects.Count - 1);
                objects[rand].transform.position = positions[i];
                if (logPositions)
                {
                    Debug.Log("sending " + objects[rand].name + " to " + positions[i]);
                }
                objects.Remove(objects[rand]);
            }
        }
    }

    void GeneratePositions()
    {
        for (int i = 0; i < objects.Count; i++)
        {
            Vector3 position = GeneratePosition();
            positions.Add(position);
        }
    }

    Vector3 GeneratePosition()
    {
        float x = Random.Range(minX, maxX);
        float z = Random.Range(minZ, maxZ);
        float y;
        RaycastHit hit;
        if (Physics.Raycast(new Vector3(x, 100, z), Vector3.down, out hit))
        {
            y = hit.point.y;
        }
        else
        {
            y = defaultHeight;
        }
        return new Vector3(x, y, z);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class BladeUIManager : MonoBehaviour
{
    public bool showNumber = false;
    public Color enabledColor; 
    public Color disabledColor; 
    BladeSpawner bladeSpawner;
    Canvas canvas;
    Canvas gunCanvas;
    GameObject panel;
    Text text;

    void Start()
    {
        bladeSpawner = GetComponent<BladeSpawner>();
        canvas = transform.Find("Canvas").GetComponent<Canvas>();
        gunCanvas = transform.Find("Weapon").GetComponent<WeaponManager>().GetActiveWeapon().transform.Find("Canvas").GetComponent<Canvas>();
        panel = transform.Find("Canvas").transform.Find("Panel").gameObject;
        if (gunCanvas.transform.Find("Text"))
        {
            text = gunCanvas.transform.Find("Text").GetComponent<Text>();
            text.enabled = showNumber;
        }
    }

    void Update()
    {

        int maxBlades = bladeSpawner.maxNumberOfBlades;
        int numBladesRemaining = bladeSpawner.GetNumBladesRemaining();
        if (text && showNumber)
        {
            gunCanvas = transform.Find("Weapon").GetComponent<WeaponManager>().GetActiveWeapon().transform.Find("Canvas").GetComponent<Canvas>();
            text = gunCanvas.transform.Find("Text").GetComponent<Text>();
            text.text = "" + numBladesRemaining;
            panel.SetActive(false);
        }
        else
        {
            if (text)
            {
                text.enabled = false;
            }
            int children = panel.transform.childCount;
            for (int i = 0; i < children; i++)
            {
                GameObject child = panel.transform.GetChild(i).gameObject;
                if (i < maxBlades)
                {
                    if (i < numBladesRemaining)
                    {
                        child.SetActive(true);
                        child.GetComponent<Image>().color = enabledColor;
                    }
                    else
                    {
                        child.SetActive(true);
                        child.GetComponent<Image>().color = disabledColor;
                    }
                }
                else
                {
                    child.SetActive(false);
                }
            }
        }

    }
}

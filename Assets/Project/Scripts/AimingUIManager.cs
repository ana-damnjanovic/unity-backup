﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimingUIManager : MonoBehaviour
{
    public bool drawTargettingLine = true;
    public Material cubeMaterial;
    public Material transparentCubeMaterial;
    public LayerMask layerMask;
    LineRenderer lineRenderer;
    float maxDistance;
    Transform topDownItemLocation;
    Transform bladeSpawnLocation;
    Transform throwLocation;
    GameObject targettingCube;
    Outline targettingCubeOutline;
    RaycastHit hit;
    BladeSpawner bladeSpawner;

    void Start()
    {
        maxDistance = GetComponent<MSDragObjects>().maxDistance;
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.positionCount = 2;
        lineRenderer.useWorldSpace = false;
        topDownItemLocation = transform.Find("TopDownHeldItemLocation");
        bladeSpawnLocation = transform.Find("BladeSpawn");
        throwLocation = transform.Find("ThrowLocation");
        targettingCube = topDownItemLocation.Find("TargettingCube").gameObject;
        targettingCubeOutline = targettingCube.GetComponent<Outline>();
        bladeSpawner = GetComponent<BladeSpawner>();
        if (!drawTargettingLine)
        {
            lineRenderer.enabled = false;
        }
    }

    void Update()
    {

        float distance = Vector3.Distance(transform.InverseTransformPoint(bladeSpawnLocation.position), transform.InverseTransformPoint(targettingCube.transform.position));
        if (Physics.Raycast(bladeSpawnLocation.position, topDownItemLocation.forward, out hit, distance + 1, layerMask))
        {
            lineRenderer.SetPosition(1, transform.InverseTransformPoint(hit.point));
        }
        else
        {
            lineRenderer.SetPosition(1, transform.InverseTransformPoint(targettingCube.transform.position));
        }
        SetTargettingCubeOutline();
    }

    void SetTargettingCubeOutline()
    {
        if (bladeSpawner.GetNumBladesRemaining() > 0)
        {
            targettingCubeOutline.OutlineMode = Outline.Mode.OutlineAndSilhouette;
            targettingCubeOutline.GetComponent<MeshRenderer>().material = cubeMaterial;
        }
        else
        {
            targettingCubeOutline.OutlineMode = Outline.Mode.OutlineAll;
            targettingCubeOutline.GetComponent<MeshRenderer>().material = transparentCubeMaterial;
        }
    }
}

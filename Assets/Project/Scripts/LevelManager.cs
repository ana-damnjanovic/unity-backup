﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    public GameObject ground;
    public GameObject ground2;
    public GameObject barrier;
    public int rebuildAfterLevels = 3;
    public List<Level> levels;
    int currentLevel;
    int numWaves;

    void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
    }

    void Start()
    {
        currentLevel = 1;
        numWaves = levels.Count;
    }

    void Update()
    {
        if (numWaves > 1)
        {
            int level = BotSpawnManager.Instance.GetCurrentWave();
            if (level != currentLevel)
            {
                currentLevel = level;
                if (currentLevel % (rebuildAfterLevels + 1) == 0)
                {
                    RebuildGround(ground);
                    RebuildGround(ground2);
                }
                SpawnBarrier();
            }
        }
    }

    void RebuildGround(GameObject ground)
    {
        if (ground)
        {
            RayFire.RayfireRigid[] rayFireChildren = ground.GetComponentsInChildren<RayFire.RayfireRigid>();
            foreach (RayFire.RayfireRigid rb in rayFireChildren)
            {
                rb.ResetRigid();
            }
        }
    }

    void SpawnBarrier()
    {
        if (barrier && ground && ground2)
        {
            float x = Random.Range(ground.transform.position.x, ground2.transform.position.x);
            float z = Random.Range(ground.transform.position.z, ground2.transform.position.z);
            float y = ground.transform.position.y + 0.5f;
            Vector3 pos = new Vector3(x, y, z);
            Instantiate(barrier, pos, Quaternion.identity);
        }
    }

    public int GetCurrentLevelNum()
    {
        return currentLevel;
    }
    
    public int GetNumWaves()
    {
        return numWaves;
    }

    public Level GetLevelValues(int i)
    {
        return levels[i];
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnlockCrate : MonoBehaviour
{
    public int ammoAmount = 5;
    [Tooltip("Objects in these layers will unlock the crate by proximity")]
    public LayerMask unlockLayers;
    public float unlockTime = 3f;
    public GameObject crateOpenEffect;
    [Tooltip("Prints debug information to the console (what object is unlocking the crate, time left to unlock, etc)")]
    public bool debugLogs = false;
    Transform unlocker;
    bool unlocked;
    float timeLeft;

    void Start()
    {
        unlocked = false;
        timeLeft = unlockTime;
    }

    void Update()
    {
        if (unlocked)
        {
            return;
        }
        if (!unlocked && unlocker)
        {
            timeLeft -= Time.deltaTime;
            if (debugLogs)
            {
                Debug.Log("unlocker is " + unlocker.name);
                Debug.Log("time left to unlock : " + timeLeft);
            }
            if (timeLeft < 0)
            {
                Unlock();
            }
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (unlockLayers == (unlockLayers | (1 << other.gameObject.layer)))
        {
            if (!unlocker)
            {
                unlocker = other.transform.root;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform.root == unlocker)
        {
            unlocker = null;
            if (!unlocked)
            {
                ResetTimer();
            }
        }
    }

    void Unlock()
    {
        if (unlocker.GetComponentInChildren<BladeSpawner>())
        {
            if (debugLogs)
            {
                Debug.Log("found bladeSpawner, adding " + ammoAmount + " ammo");
            }
            BladeSpawner spawner = unlocker.GetComponentInChildren<BladeSpawner>();
            for (int i = 0; i < ammoAmount; i++)
            {
                spawner.DecreaseNumBlades();
            }
        }
        unlocked = true;
        if (crateOpenEffect)
        {
            Instantiate(crateOpenEffect, transform.position, transform.rotation);
        }
        Destroy(transform.root.gameObject);
    }

    void ResetTimer()
    {
        timeLeft = unlockTime;
    }
}

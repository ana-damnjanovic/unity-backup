﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropOre : MonoBehaviour
{
    public float dropRate = 15f;
    public GameObject ore;
    bool terrainExists;
    void Start()
    {
        if (GameObject.Find("Terrain") || GameObject.Find("MapMagic"))
        {
            terrainExists = true;
        }
        else
        {
            terrainExists = false;
        }
    }

    public void Drop()
    {
        int rand = Random.Range(1, 100);
        if (rand < dropRate)
        {
            if (ore && terrainExists)
            {
                Instantiate(ore, transform.position, Quaternion.identity);
            }
        }
    }
}

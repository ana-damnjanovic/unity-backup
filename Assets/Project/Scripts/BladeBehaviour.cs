﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RayFire;

public class BladeBehaviour : MonoBehaviour
{
    Rigidbody bladeRb;
    Collider col;
    FixedJoint joint;
    int numCollisionsWithPlayer;
    public float forceMultiplier = 1.0f;
    public BladeSpawner spawner;
    bool forceAdded = false;
    void Start()
    {
        bladeRb = this.GetComponentInChildren<Rigidbody>();
        numCollisionsWithPlayer = 0;
    }

    void Update()
    {
        if (!forceAdded && bladeRb)
        {
            bladeRb.AddForce(transform.forward * 1000 * forceMultiplier);
            forceAdded = true;
        }
    }

    public void PickUp()
    {
        spawner.DecreaseNumBlades();
        Destroy(this.gameObject);
        Destroy(this);
    }

    public int GetNumCollisionsWithPlayer()
    {
        return numCollisionsWithPlayer;
    }

    public void IncrementNumCollisionsWithPlayer()
    {
        numCollisionsWithPlayer++;
    }

    public void UpdateForward(Vector3 target)
    {
        transform.LookAt(target);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBehaviour : MonoBehaviour
{
    int deflectionForce = 1000;
    public GameObject bladeHitShield;

    void OnTriggerEnter(Collider coll)
    {
        GameObject currentObj = coll.gameObject;
        if (currentObj.tag == "Blade")
        {
            Instantiate(bladeHitShield, currentObj.transform.position, currentObj.transform.rotation);
            if (currentObj.transform.root.name.Contains("Bullet"))
            {
                Destroy(currentObj.transform.root.gameObject);
            }
            else
            {
                currentObj.transform.root.gameObject.GetComponent<BladeBehaviour>().PickUp();
            }
        }
    }

    void OnColliderEnter(Collision coll)
    {
        GameObject currentObj = coll.gameObject;
        if (currentObj.tag == "Blade")
        {
            Instantiate(bladeHitShield, coll.contacts[0].point, currentObj.transform.rotation);
            if (currentObj.transform.root.name.Contains("Bullet"))
            {
                Destroy(currentObj.transform.root.gameObject);
            }
            else
            {
                currentObj.transform.root.gameObject.GetComponent<BladeBehaviour>().PickUp();
            }
        }
    }

    void DeflectBlade(GameObject blade)
    {
        Rigidbody[] bladeRbs = blade.GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody bladeRb in bladeRbs)
        {
            bladeRb.AddForce(this.transform.forward * deflectionForce);
        }
    }
}

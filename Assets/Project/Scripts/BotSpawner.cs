﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotSpawner : MonoBehaviour
{
    public float secondsInWave;
    public int numBombBots = 0;
    public int numShieldBots = 0;
    public int numSniperBots = 0;
    public int numInvisibleBots = 0;
    public int numDuplicatorBots = 0;
    public int numBlackHoleBots = 0;
    public int numSupportBots = 0;
    public int numAdvancedSniperBots = 0;
    public GameObject bombBot;
    public GameObject shieldBot;
    public GameObject sniperBot;
    public GameObject invisibleBot;
    public GameObject duplicatorBot;
    public GameObject blackHoleBot;
    public GameObject supportBot;
    public GameObject advancedSniperBot;
    public bool offsetX = false;
    public bool startWave;
    public bool waveFinished;
    int numBotsToSpawn;
    int numTotalBots;
    int numDeadBots;
    float botSpawnDelay;
    bool delayCalculated;
    bool isSpawning;
    void Start()
    {
        startWave = false;
        waveFinished = false;
        delayCalculated = false;
        isSpawning = false;
    }

    void Update()
    {
        if (startWave)
        {
            if (!delayCalculated)
            {
                numDeadBots = 0;
                numTotalBots = numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots + numBlackHoleBots + numSupportBots + numAdvancedSniperBots;
                numBotsToSpawn = numTotalBots;
                botSpawnDelay = secondsInWave / numBotsToSpawn;
                delayCalculated = true;
            }
            if (!isSpawning)
            {
                waveFinished = false;
                isSpawning = true;
                StartCoroutine(WaitAndSpawnBots(botSpawnDelay));
            }
            if (numDeadBots >= numTotalBots)
            {
                waveFinished = true;
                delayCalculated = false;
                startWave = false;
                isSpawning = false;
            }
        }

    }

    IEnumerator WaitAndSpawnBots(float delay)
    {
        while (numBotsToSpawn > 0)
        {
            yield return new WaitForSeconds(delay);
            SpawnRandomBot();
        }
    }

    void SpawnRandomBot()
    {
        int chance = Random.Range(0, numBotsToSpawn + 1);
        if (numBombBots > 0 && chance < numBombBots)
        {
            InstantiateBot(bombBot);
            numBombBots--;
        }        
        else if (numShieldBots > 0 && chance >= numBombBots && chance < numBombBots + numShieldBots)
        {
            InstantiateBot(shieldBot);
            numShieldBots--;
        }        
        else if (numSniperBots > 0 && chance >= numBombBots + numShieldBots && chance < numBombBots + numShieldBots + numSniperBots)
        {
            InstantiateBot(sniperBot);
            numSniperBots--;
        }        
        else if (numInvisibleBots > 0 && chance >= numBombBots + numShieldBots + numSniperBots && chance < numBombBots + numShieldBots + numSniperBots + numInvisibleBots)
        {
            InstantiateBot(invisibleBot);
            numInvisibleBots--;
        }
        else if (numDuplicatorBots > 0 && chance >= numBombBots + numShieldBots + numSniperBots + numInvisibleBots && chance < numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots)
        {
            InstantiateBot(duplicatorBot);
            numDuplicatorBots--;
        }
        else if (numBlackHoleBots > 0 && chance >= numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots && chance < numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots + numBlackHoleBots)
        {
            InstantiateBot(blackHoleBot);
            numBlackHoleBots--;
        }
        else if (numSupportBots > 0 && chance >= numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots + numBlackHoleBots && chance < numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots + numBlackHoleBots + numSupportBots)
        {
            InstantiateBot(supportBot);
            numSupportBots--;
        }
        else if (numAdvancedSniperBots > 0 && chance >= numBombBots + numShieldBots + numSniperBots + numInvisibleBots + numDuplicatorBots + numBlackHoleBots + numSupportBots + numAdvancedSniperBots)
        {
            InstantiateBot(advancedSniperBot);
            numAdvancedSniperBots--;
        }
    }

    void InstantiateBot(GameObject bot)
    {
        GameObject botInstance;
        if (offsetX)
        {
            botInstance = Instantiate(bot, new Vector3(transform.position.x + Random.Range(0, 12), transform.position.y, transform.position.z ), bot.transform.rotation);
            
        }
        else
        {
            botInstance = Instantiate(bot, new Vector3(transform.position.x, transform.position.y, transform.position.z + Random.Range(0, 12)), bot.transform.rotation);
        }
        botInstance.GetComponentInChildren<RayFire.RayfireRigid>().demolitionEvent.LocalEvent += CountDeadBots;
        if (botInstance.GetComponent<BombBodyManager>())
        {
            botInstance.GetComponent<BombBodyManager>().bombBody.GetComponentInChildren<RayFire.RayfireBomb>().explosionEvent.LocalEvent += CountDeadBots;
        }
        numBotsToSpawn--;
    }

    void CountDeadBots(RayFire.RayfireRigid rfRigid)
    {
        numDeadBots++;
    }

    void CountDeadBots(RayFire.RayfireBomb rfBomb)
    {
        numDeadBots++;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevelOnRayFireActivation : MonoBehaviour
{
    [Tooltip("Name of the scene to load")]
    public string sceneName = "Jan12";
    [Tooltip("Seconds to wait before loading scene")]
    public float loadingDelay = 2f;
    SceneLoader loader;
    RayFire.RayfireRigid rayfireRigid;
    void Start()
    {
        loader = GameObject.Find("SceneLoader").GetComponent<SceneLoader>();
        rayfireRigid = GetComponent<RayFire.RayfireRigid>();
        rayfireRigid.demolitionEvent.LocalEvent += LoadLevel;
    }

    void LoadLevel(RayFire.RayfireRigid rayfireRigid)
    {
        loader.Load(sceneName, loadingDelay);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlicedObjectGlow : MonoBehaviour
{
    public float glowFadeDuration = 1.0f;
    public Material botInteriorMaterial;
    void Start()
    {
        RayFire.RFDemolitionEvent.GlobalEvent += AddFadeGlow;
    }

    void AddFadeGlow(RayFire.RayfireRigid rayfireRigid)
    {
        GameObject obj = rayfireRigid.gameObject;
        if (obj.name.Contains("Bot") && rayfireRigid.limitations.sliceByBlade)
        {
            List<RayFire.RayfireRigid> fragments = new List<RayFire.RayfireRigid>(rayfireRigid.fragments);
            foreach (RayFire.RayfireRigid fragment in fragments)
            {
                FadeGlow glowFade = fragment.gameObject.AddComponent<FadeGlow>();
                glowFade.botInterior = botInteriorMaterial;
                glowFade.duration = glowFadeDuration;
            }
        }
    }
}

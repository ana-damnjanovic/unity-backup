﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotSpawnManager : MonoBehaviour
{
    public static BotSpawnManager Instance { get; private set; }
    public float secondsInWave = 30f;
    public float delayBetweenWaves = 5f;
    int numBombBots;
    int numShieldBots;
    int numSniperBots;
    int numInvisibleBots;
    int numDuplicatorBots;
    int numBlackHoleBots;
    int numSupportBots;
    int numAdvancedSniperBots;
    List<BotSpawner> activeChildren;
    int numRemainingWaves;
    int currentWave;
    int numWaves;
    bool settingUpWave;

    void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
    }
    void Start()
    {
        currentWave = 1;
        numWaves = LevelManager.Instance.GetNumWaves();
        numRemainingWaves = numWaves;
        GetActiveChildren();
        settingUpWave = false;
        StartCoroutine(SetUp(0f));
    }

    void GetActiveChildren()
    {
        activeChildren = new List<BotSpawner>();
        foreach (Transform child in transform)
        {
            if (child.gameObject.activeSelf)
            {
                BotSpawner spawner = child.GetComponent<BotSpawner>();
                activeChildren.Add(spawner);
                spawner.secondsInWave = secondsInWave;
            }
        }
    }

    IEnumerator SetUp(float delay)
    {
        settingUpWave = true;
        SetUpBotNumbers();
        SetUpWave();
        yield return new WaitForSeconds(delay);
        EnableSpawners();
    }

    void SetUpBotNumbers()
    {
        Level level = LevelManager.Instance.GetLevelValues(currentWave - 1);
        numBombBots = Mathf.RoundToInt(level.bombBots.RandomValue);
        numShieldBots = Mathf.RoundToInt(level.shieldBots.RandomValue);
        numSniperBots = Mathf.RoundToInt(level.sniperBots.RandomValue);
        numInvisibleBots = Mathf.RoundToInt(level.invisibleBots.RandomValue);
        numDuplicatorBots = Mathf.RoundToInt(level.duplicatorBots.RandomValue);
        numBlackHoleBots = Mathf.RoundToInt(level.blackHoleBots.RandomValue);
        numSupportBots = Mathf.RoundToInt(level.supportBots.RandomValue);
        numAdvancedSniperBots = Mathf.RoundToInt(level.advancedSniperBots.RandomValue);
    }

    void SetUpWave()
    {
        for (int i = 0; i < numBombBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numBombBots++;
        }
        for (int i = 0; i < numShieldBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numShieldBots++;
        }
        for (int i = 0; i < numSniperBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numSniperBots++;
        }
        for (int i = 0; i < numInvisibleBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numInvisibleBots++;
        }
        for (int i = 0; i < numDuplicatorBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numDuplicatorBots++;
        }
        for (int i = 0; i < numBlackHoleBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numBlackHoleBots++;
        }
        for (int i = 0; i < numSupportBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numSupportBots++;
        }
        for (int i = 0; i < numAdvancedSniperBots; i++)
        {
            activeChildren[Random.Range(0, activeChildren.Count)].numAdvancedSniperBots++;
        }
        numRemainingWaves--;
    }

    void EnableSpawners()
    {
        foreach (BotSpawner spawner in activeChildren)
        {
            spawner.startWave = true;
        }
        settingUpWave = false;
    }

    void Update()
    {
        if (numWaves == 1)
        {
            return;
        }
        else
        {
            if (WaveFinished())
            {
                if (numRemainingWaves > 0 && !settingUpWave)
                {
                    currentWave++;
                    StartCoroutine(SetUp(delayBetweenWaves));
                }
            }
        }
    }

    bool WaveFinished()
    {
        foreach (BotSpawner spawner in activeChildren)
        {
            if (spawner.waveFinished != true)
            {
                return false;
            }
        }
        return true;
    }

    public int GetCurrentWave()
    {
        return currentWave;
    }
}

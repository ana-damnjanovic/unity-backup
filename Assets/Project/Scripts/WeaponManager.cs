﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponManager : MonoBehaviour
{
    public List<GameObject> weapons = new List<GameObject>();
    [Tooltip("List the amount of ammo for each weapon on the list")]
    public List<int> ammoNums = new List<int>();
    [Tooltip("List the corresponding ammo to each weapon on the list. If two weapons use the same ammo, list it twice.")]
    public List<GameObject> ammo = new List<GameObject>();
    [Tooltip("The scroll distance required to trigger a weapon change (i.e. sensitivity)")]
    public float minScrollDistance = 0.5f;
    int currentWeapon = 0;
    int[] ammoShot;
    void Start()
    {
        weapons[currentWeapon].SetActive(true);
        ammoShot = new int[weapons.Count];
    }

    void Update()
    {
        if (Mathf.Abs(Input.mouseScrollDelta.y) > minScrollDistance)
        {
            currentWeapon++;
            if (currentWeapon > weapons.Count - 1)
            {
                currentWeapon = 0;
            }
            weapons[currentWeapon].SetActive(true);
            DisableInactiveWeapons();
        }
    }

    void DisableInactiveWeapons()
    {
        if (weapons.Count > 1)
        {
            for (int i = 0; i < weapons.Count; i++)
            {
                if (i != currentWeapon)
                {
                    weapons[i].SetActive(false);
                }
            }
        }
    }

    public GameObject GetActiveWeapon()
    {
        return weapons[currentWeapon];
    }

    public GameObject GetActiveAmmo()
    {
        if (ammo.Count > 0)
        {
            return ammo[currentWeapon];
        }
        return null;
    }
    
    public int GetAmmoMax()
    {
        return ammoNums[currentWeapon];
    }

    public int GetNumAmmoShot()
    {
        return ammoShot[currentWeapon];
    }

    public void IncrementNumAmmo()
    {
        ammoShot[currentWeapon]++;
    }

    public void DecrementNumAmmo()
    {
        if (ammoShot[currentWeapon] > 0)
        {
            ammoShot[currentWeapon]--;
        }
    }
}

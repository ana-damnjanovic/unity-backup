﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using MapMagic;
using Den.Tools;
using MapMagic.Nodes;

public class RandomizeMapSeed : MonoBehaviour
{
    public int minValue = 10000;
    public int maxValue = 50000;
    public List<Graph> graphs;
    public GameObject background;
    public MapMagic.Core.MapMagicObject mapMagic;
    GameObject desert;
    GameObject arctic;

    void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (background)
        {
            arctic = background.transform.Find("Arctic").gameObject;
            desert = background.transform.Find("Desert").gameObject;
        }
        Graph graph = graphs[0];
        int newSeed = Random.Range(minValue, maxValue);
        if (graphs.Count > 1)
        {
            int index = Random.Range(0, graphs.Count);
            graph = graphs[index];
            mapMagic.graph = graph;
            if (background)
            {
                if (graph.name.Contains("Arctic"))
                {
                    arctic.SetActive(true);
                    desert.SetActive(false);
                }
                else
                {
                    arctic.SetActive(false);
                    desert.SetActive(true);
                }
            }
        }
        Debug.Log("setting seed to " + newSeed);
        graph.random.Seed = newSeed;
        mapMagic.enabled = true;
        mapMagic.Refresh();
    }

    void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
}

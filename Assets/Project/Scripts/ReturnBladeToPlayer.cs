﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnBladeToPlayer : MonoBehaviour
{
    void OnTriggerEnter(Collider coll)
    {
        if (coll.transform.root.name.Contains("Bullet"))
        {
            Destroy(coll.transform.root.gameObject);
        }
        else if (coll.gameObject.tag == "Blade")
        {
            coll.gameObject.transform.root.GetComponent<BladeBehaviour>().PickUp();
        }
    }
}

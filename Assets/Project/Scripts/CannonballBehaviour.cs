﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonballBehaviour : MonoBehaviour
{
    public float destroyAtY = -1.2f;
    public int outlineDelay = 5;
    public int explosionDelay = 8;
    public float explosionPower = 7.0f;
    public float explosionRadius = 2.5f;
    public GameObject bigExplosionPrefab;
    public float diggerSize = 5f;
    RayFire.RayfireBomb rayfireBomb;
    Outline outline = null;
    Digger.DiggerMasterRuntime digger;
    void Start()
    {
        if (GetComponent<Outline>())
        {
            outline = GetComponent<Outline>();
            outline.enabled = false;
        }
        rayfireBomb = GetComponent<RayFire.RayfireBomb>();
        digger = GetComponent<Digger.DiggerMasterRuntime>();
        StartCoroutine(ShowOutlineAndExplode());
    }

    void Update()
    {
        if (this.transform.position.y <= destroyAtY)
        {
            Destroy(this);
        }
    }

    IEnumerator ShowOutlineAndExplode()
    {
        
        yield return new WaitForSeconds(outlineDelay);
        if (outline)
        {
            outline.enabled = true;
        }
        yield return new WaitForSeconds(explosionDelay - outlineDelay);
        Explosive explosive = this.gameObject.AddComponent(typeof(Explosive)) as Explosive;
        explosive.power = explosionPower;
        explosive.radius = explosionRadius;
        explosive.explosionPrefab = bigExplosionPrefab;
        rayfireBomb.Explode(0.0f);
        digger.Modify(this.transform.position, Digger.BrushType.Sphere, Digger.ActionType.Dig, 0, 4f, diggerSize, true, true);
        if (GetComponent<DropOre>())
        {
            GetComponent<DropOre>().Drop();
        }
        explosive.explode();
    }
}

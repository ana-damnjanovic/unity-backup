﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GivePlayerBullets : MonoBehaviour
{
    [Tooltip("the number of bullets the player gets is the Rigidbody mass times this value")]
    public float massToBulletMultiplier = 5;
    float mass;
    void Start()
    {
        if (GetComponent<Rigidbody>())
        {
            mass = this.GetComponent<Rigidbody>().mass;
            if (mass < 1)
            {
                mass = 1;
            }
        }
        else
        {
            mass = 1;
        }
    }

    public int GetNumBullets()
    {
        return (int)Mathf.Floor(massToBulletMultiplier * mass);
    }
}

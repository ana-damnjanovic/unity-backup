﻿using UnityEngine;
using System.Collections;

// this code is from this tutorial: https://bitbucket.org/BoredMormon/youtube-tutorials/src/master/UI/Fade/Fade.cs
public class FadeToBlack : MonoBehaviour
{

	public void FadeMe()
	{
		StartCoroutine(DoFade());
	}

	IEnumerator DoFade()
	{
		CanvasGroup canvasGroup = GetComponent<CanvasGroup>();
		while (canvasGroup.alpha > 0)
		{
			canvasGroup.alpha -= Time.deltaTime / 2;
			yield return null;
		}
		canvasGroup.interactable = false;
		yield return null;
	}
}

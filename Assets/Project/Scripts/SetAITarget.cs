﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetAITarget : MonoBehaviour
{
    Pathfinding.AIDestinationSetter setter;
    public GameObject playerOne;
    public GameObject playerTwo;
    public string customTargetName = "PlayerCloner";
    List<GameObject> customTargets = new List<GameObject>();
    void Awake()
    {
        if (customTargetName == "")
        {
            return;
        }
        foreach (GameObject gameObj in GameObject.FindObjectsOfType<GameObject>())
        {
            if (gameObj.name.Contains(customTargetName))
            {
                customTargets.Add(gameObj);
            }
        }
    }
    void Start()
    {
        setter = this.GetComponent<Pathfinding.AIDestinationSetter>();
        playerOne = PlayerManager.Instance.playerOne;
        playerTwo = PlayerManager.Instance.playerTwo;
    }

    void Update()
    {
        if (Time.timeScale == 1)
        {
            if (customTargets.Count == 0)
            {
                setter.target = FindClosestAlivePlayer();
            }
            else
            {
                Transform player = FindClosestAlivePlayer();
                Transform obj = FindClosestCustomTarget();
                if (DistanceToTransform(obj) < DistanceToTransform(player))
                {
                    setter.target = obj;
                }
                else
                {
                    setter.target = player;
                }
            }
        }
        else
        {
            setter.target = null;
        }
    }

    Transform FindClosestCustomTarget()
    {
        Transform closest = null;
        float minDistance = Mathf.Infinity;
        foreach (GameObject obj in customTargets)
        {
            float dist = DistanceToObject(obj);
            if (dist < minDistance)
            {
                minDistance = dist;
                closest = obj.transform;
            }
        }
        return closest;
    }

    Transform FindClosestAlivePlayer()
    {
        Transform closest = null;
        float p1Distance = Mathf.Infinity;
        float p2Distance = Mathf.Infinity;
        if (playerOne && !playerOne.GetComponent<GameOver>().IsPlayerDead())
        {
            p1Distance = DistanceToObject(playerOne);
        }
        if (playerTwo && playerTwo.activeSelf && !playerTwo.GetComponent<GameOver>().IsPlayerDead())
        {
            p2Distance = DistanceToObject(playerTwo);
        }
        if (p1Distance <= p2Distance)
        {
            closest = playerOne.transform;
        }
        if (p2Distance <= p1Distance)
        {
            closest = playerTwo.transform;
        }
        return closest;
    }

    float DistanceToObject(GameObject obj)
    {
        Vector3 diff = obj.transform.position - this.transform.position;
        return diff.sqrMagnitude;
    }

    float DistanceToTransform(Transform t)
    {
        Vector3 diff = t.position - this.transform.position;
        return diff.sqrMagnitude;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkTowardsTarget : MonoBehaviour
{
    Pathfinding.AIDestinationSetter setter;
    Pathfinding.AIPath path;
    Vector3 target;
    Rigidbody rb;
    void Start()
    {
        setter = GetComponent<Pathfinding.AIDestinationSetter>();
        rb = GetComponent<Rigidbody>();
        path = GetComponent<Pathfinding.AIPath>();
    }

    void Update()
    {
        target = path.steeringTarget;
        Move();
    }

    void Move()
    {
        Vector3 direction = target - transform.position;
        direction.Normalize();
        rb.AddForce(direction * 35);

        // https://wiki.unity3d.com/index.php/TorqueLookRotation
        Vector3 targetDelta = target - transform.position;
        float angleDiff = Vector3.Angle(-1 * transform.right, targetDelta);
        Vector3 cross = Vector3.Cross(-1 * transform.right, targetDelta);
        rb.AddTorque(new Vector3(0, cross.y * angleDiff, 0));
    }
}

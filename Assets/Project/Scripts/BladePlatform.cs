﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladePlatform : MonoBehaviour
{
    public GameObject platform;
    public float platformVerticalOffset = 1f;
    GameObject platformInstance = null;
    public KeyCode deleteKey = KeyCode.Mouse0;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Blade")
        {
            if (!platformInstance)
            {
                platformInstance = Instantiate(platform, other.transform.position + platformVerticalOffset * Vector3.up, Quaternion.identity);
            }
            else
            {
                platformInstance.transform.position = other.transform.position + platformVerticalOffset * Vector3.up;
            }
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(deleteKey))
        {
            DestroyPlatform();
        }
    }

    public void DestroyPlatform()
    {
        if (platformInstance)
        {
            Destroy(platformInstance);
        }
    }
}

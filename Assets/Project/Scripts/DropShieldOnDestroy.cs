﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropShieldOnDestroy : MonoBehaviour
{
    public GameObject shield;
    FixedJoint shieldJoint;
    void Start()
    {
        if (!shield)
        {
            shield = transform.root.Find("Shield").gameObject;
        }
        shieldJoint = shield.GetComponent<FixedJoint>();
    }

    void onDestroy()
    {
        shieldJoint.connectedBody = null;
        Destroy(shieldJoint);
        shield.GetComponent<Rigidbody>().mass = 1;
        shield.transform.parent = null;
    }
}

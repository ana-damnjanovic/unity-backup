﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BladeSplash : MonoBehaviour
{
    public GameObject splashEffect;
    public float splashVerticalOffset = 1f;

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Blade")
        {
            Instantiate(splashEffect, other.transform.position + splashVerticalOffset * Vector3.up, Quaternion.identity);
        }
    }
}

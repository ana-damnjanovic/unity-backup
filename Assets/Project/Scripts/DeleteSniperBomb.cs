﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteSniperBomb : MonoBehaviour
{
    void OnTriggerEnter(Collider coll)
    {
        if (coll.transform.root.gameObject.name.Contains("SniperBomb"))
        {
            Destroy(coll.transform.root.gameObject);
        }
    }
}

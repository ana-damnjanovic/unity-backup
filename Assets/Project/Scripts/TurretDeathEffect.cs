﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretDeathEffect : MonoBehaviour
{
    public bool destroyTurretHead = false;
    public GameObject deathEffect;

    void Start()
    {
        RayFire.RayfireRigid rfRigid = GetComponentInChildren<RayFire.RayfireRigid>();
        if (rfRigid)
        {
            rfRigid.demolitionEvent.LocalEvent += SpawnDeathEffect;
        }
    }

    void SpawnDeathEffect(RayFire.RayfireRigid rfRigid)
    {
        rfRigid.physics.useGravity = true;
        Instantiate(deathEffect, transform.position, Quaternion.identity);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Blade")
        {
            SpawnDeathEffectAndDestroy();
        }
    }

    void SpawnDeathEffectAndDestroy()
    {
        Instantiate(deathEffect, transform.position, Quaternion.identity);
        if (transform.root.GetComponent<Outline>())
        {
            transform.root.GetComponent<Outline>().enabled = false;
            Destroy(transform.root.GetComponent<Outline>());
        }
        Destroy(this.gameObject);
    }
}

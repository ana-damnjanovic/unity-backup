﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletDestruction : MonoBehaviour
{
    public LayerMask botMask;
    public LayerMask ignoreMask;
    public GameObject defaultDestructionEffect;
    public GameObject terrainDestructionEffect;
    public GameObject waterEffect;
    public bool modifyTerrain = false;
    public float diggerSize = 5f;
    bool terrainExists = false;

    void Start()
    {
        if (GameObject.Find("Terrain") || GameObject.Find("MapMagic")){
            terrainExists = true;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name.Contains("GoldCube"))
        {
            if (other.gameObject.GetComponent<GivePlayerBullets>())
            {
                AddBulletsToPlayer(other.gameObject.GetComponent<GivePlayerBullets>().GetNumBullets());
            }
        }
        else if (ignoreMask != (ignoreMask | (1 << other.gameObject.layer)))
        {
            if (terrainExists && other.gameObject.name.Contains("Terrain"))
            {
                SpawnEffectAndDestroy(true);
            }
            else if (other.gameObject.name.Contains("Water"))
            {
                SpawnEffectAndDestroy(false, true);
            }
            else
            {
                SpawnEffectAndDestroy();
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (botMask == (botMask | (1 << other.gameObject.layer)))
        {
            ReturnToPlayer();
        }
    }

    void AddBulletsToPlayer(int numBullets)
    {
        if (this.transform.root.GetComponent<BladeBehaviour>())
        {
            BladeSpawner spawner = transform.root.GetComponent<BladeBehaviour>().spawner;
            for (int i = 0; i < numBullets; i++)
            {
                spawner.DecreaseNumBlades();
            }
        }
        SpawnEffectAndDestroy();
    }

    void ReturnToPlayer()
    {
        if (this.transform.root.GetComponent<BladeBehaviour>())
        {
            this.transform.root.GetComponent<BladeBehaviour>().PickUp();
        }
        Destroy(this.gameObject);
    }

    void SpawnEffectAndDestroy(bool hitTerrain = false, bool hitWater = false)
    {
        if (defaultDestructionEffect && !hitTerrain)
        {
            Instantiate(defaultDestructionEffect, transform.position, transform.rotation);
        }
        if (terrainExists)
        {
            if (modifyTerrain)
            {
                if (GetComponent<DropOre>())
                {
                    GetComponent<DropOre>().Drop();
                }
                if (GetComponent<Digger.DiggerMasterRuntime>())
                {
                    GetComponent<Digger.DiggerMasterRuntime>().Modify(this.transform.position, Digger.BrushType.Sphere, Digger.ActionType.Dig, 0, 4f, diggerSize, true, true);
                }
            }
            if (hitTerrain && terrainDestructionEffect)
            {
                Instantiate(terrainDestructionEffect, transform.position, transform.rotation);
            }
        }
        if (waterEffect && hitWater)
        {
            Instantiate(waterEffect, transform.position, transform.rotation);
        }
        Destroy(this.gameObject);
    }
}

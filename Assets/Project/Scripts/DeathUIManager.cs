﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 

public class DeathUIManager : MonoBehaviour
{
    public GameObject playerOne;
    public GameObject playerTwo;
    public Camera p1Camera;
    public Camera p2Camera;
    public GameObject topDownCamera;
    public GameObject gameOverScreen;
    public GameObject reviveTimerScreen;
    public float timeToRevive = 6.0f;
    bool isP1Dead;
    bool isP2Dead;
    Camera gameOverCamera;
    GameObject gameOverScreenUI;
    void Awake()
    {
        if (!gameOverScreen)
        {
            gameOverScreen = transform.Find("GameOverScreen").gameObject;
        }
        if (!reviveTimerScreen)
        {
            reviveTimerScreen = transform.Find("ReviveTimerScreen").gameObject;
        }
        if (!topDownCamera)
        {
            topDownCamera = GameObject.Find("TopDownCamera");
        }
    }
    void Start()
    {
        playerOne = PlayerManager.Instance.playerOne;
        playerTwo = PlayerManager.Instance.playerTwo;
        p1Camera = PlayerManager.Instance.p1Camera;
        p2Camera = PlayerManager.Instance.p2Camera;
        if (!gameOverCamera)
        {
            gameOverCamera = gameOverScreen.transform.Find("GameOverCamera").GetComponent<Camera>();
        }
        if (!gameOverScreenUI)
        {
            gameOverScreenUI = gameOverScreen.transform.Find("Canvas").gameObject;
        }

        //p1Camera.enabled = true;
        gameOverCamera.enabled = false;
        gameOverScreenUI.SetActive(false);
        if (reviveTimerScreen)
        {
            reviveTimerScreen.SetActive(false);
        }
    }

    void AdjustGameOverCamera(Vector3 position, Quaternion rotation)
    {
        gameOverCamera.transform.position = position;
        gameOverCamera.transform.rotation = rotation;
    }

    public void OnPlayerDeath()
    {
        if (PlayerManager.Instance.gameObject.GetComponent<TogglePlayerTwo>())
        {
            PlayerManager.Instance.gameObject.GetComponent<TogglePlayerTwo>().enabled = false;
        }
        bool isP1Dead = playerOne.GetComponentInChildren<GameOver>().IsPlayerDead();
        bool isP2Dead = playerTwo.GetComponentInChildren<GameOver>().IsPlayerDead();
        if (!playerTwo.activeSelf && isP1Dead)
        {
            StartCoroutine(WaitAndShowP1GameOverScreen(p1Camera, gameOverCamera));
        }
        else if ((!isP1Dead && isP2Dead))
        {
            StartCoroutine(WaitAndToggleCameras(p1Camera, p2Camera, playerTwo));
        }
        else if ((isP1Dead && !isP2Dead))
        {
            StartCoroutine(WaitAndToggleCameras(p2Camera, p1Camera, playerOne));
        }
        else
        {
            StartCoroutine(WaitAndShowGameOverScreen(p1Camera, p2Camera, gameOverCamera));
        }
    }

    IEnumerator WaitAndToggleCameras(Camera aliveCamera, Camera deadCamera, GameObject playerToRevive)
    {
        if (reviveTimerScreen)
        {
            playerToRevive.GetComponentInChildren<GameOver>().SetCanRevive(true);
            yield return StartCoroutine(ShowReviveScreen(deadCamera));
            playerToRevive.GetComponentInChildren<GameOver>().SetCanRevive(false);
            reviveTimerScreen.SetActive(false);
        }
        if (playerToRevive.GetComponent<GameOver>().IsPlayerDead())
        {
            ShowPlayerDeathScreen(playerToRevive);
            if (topDownCamera && topDownCamera.activeSelf)
            {
                AdjustGameOverCamera(topDownCamera.transform.position, topDownCamera.transform.rotation);
            }
            else
            {
                AdjustGameOverCamera(aliveCamera.transform.position, aliveCamera.transform.rotation);
                deadCamera.rect = new Rect(0f, 0.5f, 0f, 0f);
                aliveCamera.rect = new Rect(0f, 0f, 1f, 1f);
            }
        }
    }

    IEnumerator WaitAndShowP1GameOverScreen(Camera p1Camera, Camera gameOverCamera)
    {
        if (topDownCamera && topDownCamera.activeSelf)
        {
            AdjustGameOverCamera(topDownCamera.transform.position, topDownCamera.transform.rotation);
        }
        else
        {
            AdjustGameOverCamera(p1Camera.transform.position, p1Camera.transform.rotation);
        }
        yield return new WaitForSeconds(1);
        p1Camera.enabled = false;
        p2Camera.enabled = false;
        if (topDownCamera)
        {
            topDownCamera.SetActive(false);
        }
        gameOverCamera.enabled = true;
        gameOverScreenUI.SetActive(true);
        Time.timeScale = 0;
    }

    IEnumerator WaitAndShowGameOverScreen(Camera p1Camera, Camera p2Camera, Camera gameOverCamera)
    {
        yield return new WaitForSeconds(2);
        p1Camera.enabled = false;
        p2Camera.enabled = false;
        if (topDownCamera)
        {
            topDownCamera.SetActive(false);
        }
        gameOverCamera.enabled = true;
        gameOverScreenUI.SetActive(true);
        Time.timeScale = 0;
    }

    IEnumerator ShowReviveScreen(Camera camera)
    {
        if (topDownCamera && !topDownCamera.activeSelf)
        {
            GameObject player = camera.transform.root.gameObject;
            reviveTimerScreen.SetActive(true);
            reviveTimerScreen.GetComponentInChildren<Canvas>().worldCamera = camera;
            Text timerLabel = reviveTimerScreen.GetComponentInChildren<Text>();
            float currCountdownValue = timeToRevive;
            while (currCountdownValue > 0)
            {
                if (!player.GetComponent<GameOver>().IsPlayerDead())
                {
                    reviveTimerScreen.SetActive(false);
                    break;
                }
                timerLabel.text = string.Format("{0}", currCountdownValue);
                yield return new WaitForSeconds(1.0f);
                currCountdownValue--;
            }
        }
}

    IEnumerator ShowPlayerDeathScreen(GameObject player)
    {
        GameObject deathScreen = player.transform.Find("DeathScreen").gameObject;
        if (deathScreen)
        {
            deathScreen.SetActive(true);
            yield return new WaitForSeconds(1);
            deathScreen.SetActive(false);
        }
    }

}

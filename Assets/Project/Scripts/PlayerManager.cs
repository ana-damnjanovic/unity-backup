﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public GameObject playerOne;
    public GameObject playerTwo;
    public Camera p1Camera;
    public Camera p2Camera;
    public static PlayerManager Instance { get; private set; } // static singleton
    void Awake()
    {
        if (Instance == null) { Instance = this; }
        else { Destroy(gameObject); }
        foreach (GameObject gameObject in Resources.FindObjectsOfTypeAll(typeof(GameObject)) as GameObject[])
        {
            if (!playerTwo && gameObject.name == "PlayerTwo")
            {
                playerTwo = gameObject;
            }
            if (!playerOne && gameObject.name == "PlayerOne")
            {
                playerOne = gameObject;
            }
            if (playerOne && playerTwo)
            {
                 break;
            }
        }
        p1Camera = playerOne.GetComponentInChildren<Camera>();
        if (playerTwo)
        {
            p2Camera = playerTwo.GetComponentInChildren<Camera>();
        }
    }
}

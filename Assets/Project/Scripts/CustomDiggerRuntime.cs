﻿using UnityEngine;

namespace Digger
{
    #region DiggerPRO
    public class CustomDiggerRuntime : MonoBehaviour
    {
        public BrushType brush = BrushType.Sphere;
        public KeyCode digKey = KeyCode.Mouse0;
        public KeyCode addKey = KeyCode.Mouse1;

        public LayerMask diggerLayerMask = Physics.DefaultRaycastLayers;
        public LayerMask doNotOverlap = Physics.DefaultRaycastLayers;
        public float verticalOffset = 0f;
        [Range(0, 7)] public int textureIndex;
        [Range(0.5f, 10f)] public float size = 4f;
        [Range(0f, 1f)] public float opacity = 0.5f;
        public bool autoRemoveTrees = true;
        public bool autoRemoveDetails = true;

        [Header("Persistence parameters (make sure persistence is enabled in Digger Master Runtime)")]
        public KeyCode keyToPersistData = KeyCode.P;

        public KeyCode keyToDeleteData = KeyCode.K;

        [Header("Editor-only parameters")]
        [Tooltip("Enable to remove trees while you dig in Play Mode in the Unity editor. " +
                 "CAUTION: removal is permanent and will remain after you exit Play Mode!")]
        public bool autoRemoveTreesInEditor = false;

        [Tooltip("Enable to remove grass and details while you dig in Play Mode in the Unity editor. " +
                 "CAUTION: removal is permanent and will remain after you exit Play Mode!")]
        public bool autoRemoveDetailsInEditor = false;

        private DiggerMasterRuntime diggerMasterRuntime;

        Vector3 offset;

        private void Start()
        {
            offset = new Vector3(0, verticalOffset, 0);
            diggerMasterRuntime = FindObjectOfType<DiggerMasterRuntime>();
            if (!diggerMasterRuntime)
            {
                enabled = false;
                Debug.LogWarning(
                    "DiggerRuntimeUsageExample component requires DiggerMasterRuntime component to be setup in the scene. DiggerRuntimeUsageExample will be disabled.");
                return;
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(digKey))
            {
                // Perform a raycast to find terrain surface and call Modify method of DiggerMasterRuntime to edit it
                if (DiggerPhysics.Raycast(transform.position + offset, transform.forward, out var hit, 2000f, diggerLayerMask))
                {
                    if (hit.transform.root.name == "MapMagic")
                    {
                        diggerMasterRuntime.Modify(hit.point, brush, ActionType.Dig, textureIndex, opacity, size,
                            Application.isEditor ? autoRemoveDetailsInEditor : autoRemoveDetails,
                            Application.isEditor ? autoRemoveTreesInEditor : autoRemoveTrees);
                    }

                }
            }
            if (Input.GetKey(addKey))
            {
                if (DiggerPhysics.Raycast(transform.position + offset, transform.forward, out var hit, 2000f, diggerLayerMask) && Vector3.Distance(hit.point, transform.position) > 2)
                {
                    if (hit.transform.root.name == "MapMagic")
                    {
                        if (!Physics.CheckSphere(hit.point, size, doNotOverlap)){
                            diggerMasterRuntime.Modify(hit.point, brush, ActionType.Add, textureIndex, opacity, size,
                                Application.isEditor ? autoRemoveDetailsInEditor : autoRemoveDetails,
                                Application.isEditor ? autoRemoveTreesInEditor : autoRemoveTrees);
                        }

                    }
                }
            }

            if (Input.GetKeyDown(keyToPersistData))
            {
                diggerMasterRuntime.PersistAll();
                #if !UNITY_EDITOR
                Debug.Log("Persisted all modified chunks");
                #endif
            }
            else if (Input.GetKeyDown(keyToDeleteData))
            {
                diggerMasterRuntime.DeleteAllPersistedData();
                #if !UNITY_EDITOR
                Debug.Log("Deleted all persisted data");
                #endif
            }
        }
    }

    #endregion
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyAtHeight : MonoBehaviour
{
    public float yHeight = -0.5f;
    void Update()
    {
        if (this.transform.position.y < yHeight)
        {
            Destroy(this.gameObject);
        }
    }
}

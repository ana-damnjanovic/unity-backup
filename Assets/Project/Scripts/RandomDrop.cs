﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomDrop : MonoBehaviour
{
    public GameObject extraBladePrefab;
    public float extraBladeDropRate = 5f;
    public GameObject invincibilityPrefab;
    public float invincibilityDropRate = 5f;
    public GameObject rewindPrefab;
    public float rewindDropRate = 5f;
    bool isQuitting = false;
    RayFire.RayfireRigid rfRigid;

    void Start()
    {
        List<float> rates = new List<float>() { extraBladeDropRate, invincibilityDropRate, rewindDropRate };
        if ((extraBladeDropRate + invincibilityDropRate + rewindDropRate) > 100f)
        {
            float maxValue = Mathf.Max(extraBladeDropRate, invincibilityDropRate, rewindDropRate);
            rates.Remove(maxValue);
            maxValue = 100f - (rates[0] + rates[1]);
        }
        rfRigid = this.GetComponent<RayFire.RayfireRigid>();
        rfRigid.demolitionEvent.LocalEvent += SpawnRandomDrop;
    }

    void OnApplicationQuit()
    {
        isQuitting = true;
    }

    void SpawnRandomDrop(RayFire.RayfireRigid rfRigid)
    {
        if (!isQuitting)
        {
            float chance = Random.Range(0f, 100f);
            if (chance < extraBladeDropRate)
            {
                Instantiate(extraBladePrefab, this.transform.position, Quaternion.identity);
            }
            else if (chance >= extraBladeDropRate && chance < (extraBladeDropRate + invincibilityDropRate))
            {
                Instantiate(invincibilityPrefab, this.transform.position, Quaternion.identity);
            }
            else if (chance >= (extraBladeDropRate + invincibilityDropRate) && chance < (extraBladeDropRate + invincibilityDropRate + rewindDropRate))
            {
                Instantiate(rewindPrefab, this.transform.position, Quaternion.identity);
            }
        }
    }
}

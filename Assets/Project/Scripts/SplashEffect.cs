﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashEffect : MonoBehaviour
{
    public GameObject splash;
    bool instantiated = false;
    LayerMask layerMask;
    RaycastHit hit;

    void Start()
    {
        layerMask = LayerMask.NameToLayer("Water");
    }

    void Update()
    {
        if (instantiated)
        {
            return;
        }
        else
        {
            if (Physics.Raycast(this.transform.position, Vector3.down, out hit, 1) && hit.transform.gameObject.layer == layerMask)
            {
                GameObject splashInstance = Instantiate(splash, transform.position, Quaternion.identity, this.transform);
                instantiated = true;
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableBombBodyOnCollision : MonoBehaviour
{
    BombBodyManager manager;

    void Start()
    {
        manager = this.transform.root.GetComponent<BombBodyManager>();
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            manager.EnableBombBody();
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatchFragments : MonoBehaviour
{
	void OnTriggerEnter(Collider col)
    {
		DestroyNonFragments(col);
    }
	
	void OnTriggerStay(Collider col)
    {
		DestroyNonFragments(col);
    }

	void OnTriggerExit(Collider coll)
    {
		Rigidbody rb = coll.gameObject.GetComponent<Rigidbody>();
		if (rb)
        {
			rb.constraints = RigidbodyConstraints.None;
		}
	}

	void DestroyNonFragments(Collider col)
	{
		GameObject currentItem = col.gameObject;
		Rigidbody rb = currentItem.GetComponent<Rigidbody>();
		if (rb)
        {
			//rb.isKinematic = true;
			rb.constraints = RigidbodyConstraints.FreezeAll;
        }
		else
        {
			Destroy(currentItem);
        }
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnMapLoad : MonoBehaviour
{
    public MapMagic.Core.MapMagicObject mapMagic;

    void Update()
    {
        if (mapMagic && !mapMagic.IsGenerating())
        {
            Destroy(this.gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableRandomCenter : MonoBehaviour
{
    public List<GameObject> centers;
    void Start()
    {
        if (centers.Count > 0)
        {
            PickRandomCenter();
        }
    }

    void PickRandomCenter()
    {
        int rand = Random.Range(0, centers.Count - 1);
        for (int i = 0; i < centers.Count; i++) 
        {
            if (i == rand)
            {
                centers[i].SetActive(true);
            }
            else
            {
                centers[i].SetActive(false);
            }
        }
    }
}

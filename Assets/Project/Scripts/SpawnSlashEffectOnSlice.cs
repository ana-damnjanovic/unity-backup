﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSlashEffectOnSlice : MonoBehaviour
{
    public GameObject slashPrefab;
    RayFire.RayfireBlade rayfireBlade;
    RayFire.RFSliceEvent sliceEvent;

    void Start()
    {
        rayfireBlade = GetComponent<RayFire.RayfireBlade>();
        sliceEvent = rayfireBlade.sliceEvent;
        sliceEvent.LocalEvent += SpawnSlashEffect;
    }

    public void SpawnSlashEffect(RayFire.RayfireBlade blade)
    {
        GameObject slashInstance = Instantiate(slashPrefab, this.transform.position, this.transform.rotation);
        slashInstance.transform.parent = this.transform.root;
    }
}

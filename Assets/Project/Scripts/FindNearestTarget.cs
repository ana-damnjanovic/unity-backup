﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class FindNearestTarget : MonoBehaviour
{
    public float radius = 5f;
    public LayerMask layerMask;
    Transform target;
    LookAtConstraint lookAtConstraint;
    ConstraintSource source;
    int sourceIndex;
    RaycastHit hit;
    ProjectileSpawn spawner;
    Rigidbody rb;

    void Start()
    {
        lookAtConstraint = this.GetComponent<LookAtConstraint>();
        spawner = GetComponentInChildren<ProjectileSpawn>();
        spawner.enabled = false;
        rb = GetComponent<Rigidbody>();
        rb.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void Update()
    {
        if (target)
        {
            rb.constraints = RigidbodyConstraints.None;
            if (Vector3.Distance(transform.position, target.transform.position) > radius)
            {
                lookAtConstraint.RemoveSource(sourceIndex);
                target = null;
            }
        }
        if (!target)
        {
            rb.constraints = RigidbodyConstraints.FreezeRotation;
            if (lookAtConstraint.sourceCount > 0)
            {
                lookAtConstraint.RemoveSource(sourceIndex);
            }
            spawner.enabled = false;
            GetNearestTarget();
        }
    }

    void GetNearestTarget()
    {
        Collider[] targets = Physics.OverlapSphere(this.transform.position, radius, layerMask);
        if (targets.Length == 0)
        {
            spawner.enabled = false;
        }
        if (targets.Length > 0)
        {
            spawner.enabled = true;
            target = targets[0].transform;
            source.sourceTransform = target.transform;
            source.weight = 1;
            lookAtConstraint.AddSource(source);
            sourceIndex = lookAtConstraint.sourceCount - 1;
        }
    }
}

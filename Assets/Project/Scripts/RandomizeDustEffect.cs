﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeDustEffect : MonoBehaviour
{
    public GameObject dustEffectPrefab;
    [Range(1.0f, 100.0f)][Tooltip("The % chance that the effect will play.")]
    public float chanceOfEffect = 100.0f;
    GameObject dustEffectInstance;
    ParticleSystem dustEffect;
    void Start()
    {
        if (dustEffectPrefab)
        {
            dustEffectInstance = Instantiate(dustEffectPrefab, transform.position, Quaternion.identity);
            dustEffect = dustEffectInstance.GetComponent<ParticleSystem>();
        }
    }
    void OnTriggerEnter(Collider other)
    {
        float randValue = Random.value;
        float chance = chanceOfEffect / 100.0f;
        if (other.gameObject.tag == "Destructible" && randValue < chance)
        {
            if (!dustEffect.isPlaying)
            {
                dustEffectInstance.transform.position = other.ClosestPointOnBounds(transform.position);
                dustEffect.Play();
            }
        }
    }
}

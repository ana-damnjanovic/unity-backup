﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelUIManager : MonoBehaviour
{
    public GameObject directionalLight;
    public GameObject levelDisplay;
    Text levelText;
    int currentLevel;
    LevelManager levelManager;

    void Start()
    {
        currentLevel = 1;
        if (!levelDisplay)
        {
            GameObject.Find("LevelDisplay");
        }
        levelText = levelDisplay.transform.Find("Canvas").Find("Level Image").GetComponentInChildren<Text>();
        levelManager = GetComponent<LevelManager>();
    }

    void Update()
    {
        if(levelManager.GetNumWaves() > 1)
        {
            int level = levelManager.GetCurrentLevelNum();
            if (level != currentLevel)
            {
                RandomizeLightColor();
                currentLevel = level;
                levelText.text = currentLevel.ToString();
            }
        }
    }

    void RandomizeLightColor()
    {
        if (directionalLight)
        {
            Light light = directionalLight.GetComponent<Light>();
            float hue;
            float saturation;
            float value;
            Color.RGBToHSV(light.color, out hue, out saturation, out value);
            light.color = Random.ColorHSV(0f, 1f, saturation, saturation, value, value);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TopDownMoveObjects : MonoBehaviour
{
    public KeyCode KeyToMove = KeyCode.Mouse0;
    public KeyCode KeyToThrow = KeyCode.Mouse1;
    public float throwingForce = 800;
    public bool drawTargettingLine = true;
    public Color targettingCubeDefault;
    public Color targettingCubeSelected;
    LineRenderer lineRenderer;
    float maxDistance;
    Transform topDownItemLocation;
    GameObject targettingCube;
    Vector3 rayCastOffset;
    RaycastHit hit;
    bool objectThrown;
    GameObject heldItem;
    Quaternion heldItemRotation;
    Rigidbody heldItemRb;
    BladeSpawner bladeSpawner;

    void Start()
    {
        heldItem = null;
        heldItemRb = null;
        objectThrown = false;
        maxDistance = GetComponent<MSDragObjects>().maxDistance;
        throwingForce = GetComponent<MSDragObjects>().throwingForce;
        lineRenderer = GetComponent<LineRenderer>();

        lineRenderer.positionCount = 2;
        lineRenderer.useWorldSpace = false;
        topDownItemLocation = transform.Find("TopDownHeldItemLocation");
        targettingCube = topDownItemLocation.Find("TargettingCube").gameObject;
        targettingCube.GetComponent<Renderer>().material.color = targettingCubeDefault;
        bladeSpawner = GetComponent<BladeSpawner>();
        rayCastOffset = new Vector3(0.0f, -1.0f, 0.0f);
        if (!drawTargettingLine)
        {
            lineRenderer.enabled = false;
        }
    }

    void Update()
    {
        GameObject potentialItem = null;
        if (RayCastArray(out hit))
        {
            float distance = Vector3.Distance(transform.position + rayCastOffset, hit.point);
            if (distance <= maxDistance && hit.transform.CompareTag("Destructible") && hit.transform.gameObject.layer != LayerMask.NameToLayer("Bot"))
            {
                potentialItem = hit.transform.gameObject;
            }
        }
        SetTargettingCubeColor(potentialItem);
        if (Input.GetKeyDown(KeyToMove))
        {
            if (!heldItem && potentialItem)
            {
                bladeSpawner.SetCanSpawn(false);
                ParentAndDisableColliders(potentialItem);
                heldItem = potentialItem;
                heldItemRotation = heldItem.transform.rotation;
                heldItemRb = heldItem.GetComponent<Rigidbody>();
                heldItemRb.collisionDetectionMode = CollisionDetectionMode.ContinuousSpeculative;
                heldItemRb.isKinematic = true;
                targettingCube.GetComponent<MeshRenderer>().enabled = false;
            }
            else if (heldItem)
            {
                UnParentAndEnableColliders(heldItem);
                heldItemRb.isKinematic = false;
                heldItem = null;
                bladeSpawner.SetCanSpawn(true);
            }
        }
        if (Input.GetKeyDown(KeyToThrow) && heldItem)
        {
            UnParentAndEnableColliders(heldItem);
            heldItemRb.isKinematic = false;
            heldItemRb.AddForce(topDownItemLocation.transform.forward * throwingForce * 4);
            heldItem = null;
            objectThrown = true;
        }
        if (Input.GetKeyUp(KeyToThrow) && objectThrown)
        {
            bladeSpawner.SetCanSpawn(true);
            objectThrown = false;
        }
        if (drawTargettingLine)
        {
            lineRenderer.SetPosition(1, transform.InverseTransformPoint(targettingCube.transform.position));
        }
        Debug.DrawRay(this.transform.position + rayCastOffset, topDownItemLocation.transform.forward * maxDistance, Color.green);
        Debug.DrawRay(this.transform.position + 1.25f * rayCastOffset, topDownItemLocation.transform.forward * maxDistance, Color.green);
        Debug.DrawRay(this.transform.position + 1.5f * rayCastOffset, topDownItemLocation.transform.forward * maxDistance, Color.green);
        Debug.DrawRay(this.transform.position + 1.75f * rayCastOffset, topDownItemLocation.transform.forward * maxDistance, Color.green);
        Debug.DrawRay(this.transform.position + 2f * rayCastOffset, topDownItemLocation.transform.forward * maxDistance, Color.green);

    }

    void LateUpdate()
    {
        if (!heldItem)
        {
            targettingCube.GetComponent<MeshRenderer>().enabled = true;
        }
    }

    bool RayCastArray(out RaycastHit hit)
    {
        if (Physics.Raycast(transform.position + rayCastOffset, topDownItemLocation.transform.forward, out hit, (maxDistance + 1)))
        {
            return true;
        }
        else if (Physics.Raycast(transform.position + 1.25f * rayCastOffset, topDownItemLocation.transform.forward, out hit, (maxDistance + 1)))
        {
            return true;
        }
        else if (Physics.Raycast(transform.position + 1.5f * rayCastOffset, topDownItemLocation.transform.forward, out hit, (maxDistance + 1)))
        {
            return true;
        }
        else if (Physics.Raycast(transform.position + 1.75f * rayCastOffset, topDownItemLocation.transform.forward, out hit, (maxDistance + 1)))
        {
            return true;
        }
        else if (Physics.Raycast(transform.position + 2f * rayCastOffset, topDownItemLocation.transform.forward, out hit, (maxDistance + 1)))
        {
            return true;
        }
        return false;
    }

    void SetTargettingCubeColor(GameObject potentialItem)
    {
        if (potentialItem != null)
        {
            targettingCube.GetComponent<Renderer>().material.SetColor("_Color", targettingCubeSelected);
        }
        else
        {
            targettingCube.GetComponent<Renderer>().material.color = targettingCubeDefault;
        }
    }

    void ParentAndDisableColliders(GameObject heldObject)
    {
        heldObject.transform.parent = topDownItemLocation;
        heldObject.transform.position = topDownItemLocation.position + 1.25f * topDownItemLocation.forward;
        Collider coll = heldObject.GetComponent<Collider>();
        if (coll)
        {
            coll.enabled = false;
        }
    }

    void UnParentAndEnableColliders(GameObject heldObject)
    {
        heldObject.transform.rotation = heldItemRotation;
        heldObject.transform.parent = null;
        Collider coll = heldObject.GetComponent<Collider>();
        if (coll)
        {
            coll.enabled = true;
        }
    }
}

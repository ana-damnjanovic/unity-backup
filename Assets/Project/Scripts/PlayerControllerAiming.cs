﻿using UnityEngine;
using System.Collections;

public class PlayerControllerAiming : MonoBehaviour
{
    public string windowsXAxis = "Axis 4";
    public string windowsYAxis = "Axis 5";
    public string macXAxis = "Axis 3";
    public string macYAxis = "Axis 4";
    string xAxis;
    string yAxis;
    public bool useMacControls = false;
    public Transform topDownBladeSpawn;
    // from https://answers.unity.com/questions/631897/right-stick-aiming-problem-dual-stick-shooter.html
    // and https://answers.unity.com/questions/932115/top-down-aim-at-mouse.html

    void Start()
    {
        if (!topDownBladeSpawn)
        {
            topDownBladeSpawn = transform.parent.Find("BladeSpawn");
        }
        if (useMacControls)
        {
            xAxis = macXAxis;
            yAxis = macYAxis;
        }
        else
        {
            xAxis = windowsXAxis;
            yAxis = windowsYAxis;
        }
    }
    void Update()
    {
        // We are going to read the input every frame
        Vector3 vNewInput = new Vector3(Input.GetAxis(xAxis), Input.GetAxis(yAxis), 0.0f);

        // Only do work if meaningful
        if (vNewInput.sqrMagnitude < 0.5f)
        {
            return;
        }
        float angle = Mathf.Atan2(Input.GetAxis(yAxis), Input.GetAxis(xAxis)) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0, angle + 180, 0);
        topDownBladeSpawn.rotation = Quaternion.Euler(0, angle + 180, 0);
    }
}
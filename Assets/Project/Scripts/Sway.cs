﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sway : MonoBehaviour
{
    /* based on tutorial from https://www.youtube.com/watch?v=udiUKT5E4yE  */
    float amount = 0.055f;
    float maxAmount = 0.09f;
    float smooth = 3f;
    Vector3 pos;
    Vector3 euler;

    void Start()
    {
        pos = transform.localPosition;
        euler = transform.localEulerAngles;
    }

    float _smooth;

    void Update()
    {
        float factorX = -Input.GetAxis("Mouse X") * amount;
        float factorY = -Input.GetAxis("Mouse Y") * amount;

        if (factorX > maxAmount)
            factorX = maxAmount;

        if (factorX < -maxAmount)
            factorX = -maxAmount;

        if (factorY > maxAmount)
            factorY = maxAmount;

        if (factorY < -maxAmount)
            factorY = -maxAmount;

        Vector3 final = new Vector3(pos.x + factorX, pos.y + factorY, pos.z);
        transform.localPosition = Vector3.Lerp(transform.localPosition, final, Time.deltaTime * smooth);
    }
}

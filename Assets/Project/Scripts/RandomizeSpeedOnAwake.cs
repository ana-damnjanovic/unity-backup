﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomizeSpeedOnAwake : MonoBehaviour
{
    public int minSpeed = 1;
    public int maxSpeed = 5;
    void Awake()
    {
        float speed = Random.Range(minSpeed, (maxSpeed + 1));
        this.GetComponent<Pathfinding.AIPath>().maxSpeed = (float)speed;
    }
}

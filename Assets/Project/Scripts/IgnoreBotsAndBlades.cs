﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreBotsAndBlades : MonoBehaviour
{
    [Tooltip("Ignores the Bot layer this many seconds after being spawned.")]
    public float ignoreDelay = 1.0f;
    bool ignoreBots;
    Collider pickupCollider; 

    void Start()
    {
        ignoreBots = false;
        StartCoroutine(WaitAndIgnoreBots());
        pickupCollider = GetComponent<Collider>();
    }

    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.layer == LayerMask.NameToLayer("Bot") && ignoreBots)
        {
            Physics.IgnoreCollision(pickupCollider, coll.collider);
        }
        if (coll.gameObject.tag == "Blade")
        {
            Physics.IgnoreCollision(pickupCollider, coll.collider);
        }
    }

    IEnumerator WaitAndIgnoreBots()
    {
        yield return new WaitForSeconds(ignoreDelay);
        ignoreBots = true;
    }
}

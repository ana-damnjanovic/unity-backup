﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePlayerTwo : MonoBehaviour
{
    public GameObject playerTwo;
    public KeyCode toggleKey = KeyCode.P;
    Camera p1Camera;

    void Start()
    {
        if (!playerTwo)
        {
            playerTwo = PlayerManager.Instance.playerTwo;
        }
        p1Camera = PlayerManager.Instance.p1Camera;
        ResizeCameraRect();
    }

    void Update()
    {
        if (!playerTwo.activeSelf && AnyControllerButtonPressed())
        {
            playerTwo.SetActive(true);
            ResizeCameraRect();
        }
        if (Input.GetKeyDown(toggleKey))
        {
            playerTwo.SetActive(!playerTwo.activeSelf);
            ResizeCameraRect();
        }
    }

    bool AnyControllerButtonPressed()
    {
        for (int i = 0; i < 6; i++)
        {
            if (Input.GetKeyDown("joystick button " + i)){
                return true;
            }
        }
        return false;
    }

    void ResizeCameraRect()
    {
        if (playerTwo.activeSelf)
        {
            p1Camera.rect = new Rect(0f, 0.5f, 1f, 0.5f);
        }
        else
        {
            p1Camera.rect = new Rect(0f, 0f, 1f, 1f);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class RemoveEmptyPositionConstraint : MonoBehaviour
{
    PositionConstraint constraint;
    bool adjusted = false;
    void Start()
    {
        constraint = GetComponent<PositionConstraint>();
    }

    void Update()
    {
        if (!adjusted && constraint.GetSource(0).sourceTransform == null)
        {
            constraint.RemoveSource(0);
            adjusted = true;
        }
    }
}

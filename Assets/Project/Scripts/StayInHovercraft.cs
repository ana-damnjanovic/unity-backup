﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayInHovercraft : MonoBehaviour
{
    public GameObject hovercraft;
    public string steeringWheelName = "HovercraftSteering";
    MSDragObjects moveObjects;
    bool parentedToHovercraft;
    public Transform heldItemLocation;
    HovercraftBehaviour hovercraftBehaviour;
    GameObject hovercraftBody;
    void Start()
    {
        moveObjects = GetComponent<MSDragObjects>();
        parentedToHovercraft = false;
        hovercraftBehaviour = hovercraft.GetComponent<HovercraftBehaviour>();
        hovercraftBody = hovercraft.transform.GetChild(0).gameObject;
    }

    void Update()
    {
        if (heldItemLocation.childCount > 0)
        {
            GameObject obj = heldItemLocation.GetChild(0).gameObject;
            if (obj.name == steeringWheelName)
            {
                if (!parentedToHovercraft)
                {
                    hovercraftBehaviour.ParentPlayer(this.gameObject);
                    parentedToHovercraft = true;
                }
                transform.position = hovercraftBody.transform.position + Vector3.up;
            }
        }
        else
        {
            hovercraftBehaviour.UnparentPlayer(this.gameObject);
            parentedToHovercraft = false;
        }

    }
}

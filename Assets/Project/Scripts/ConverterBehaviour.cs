﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConverterBehaviour : MonoBehaviour
{
    public float requiredMass = 10f;
    public GameObject resultObject;
    public float receivedMass;
    public Material materialToConvert;
    void Start()
    {
        receivedMass = 0f;    
    }
    
    void OnCollisionEnter(Collision coll)
    {
        if (coll.gameObject.tag == "Destructible")
        {
            if (coll.gameObject.GetComponent<MeshRenderer>().sharedMaterial == materialToConvert)
            {
                CollectMass(coll.gameObject);
                Destroy(coll.gameObject);
            }
        }
    }

    void Update()
    {
        if (receivedMass >= requiredMass)
        {
            Instantiate(resultObject, transform.position + Vector3.up, Quaternion.identity);
            receivedMass -= requiredMass;
        }
    }

    void CollectMass(GameObject obj)
    {
        receivedMass += obj.GetComponent<Rigidbody>().mass;
    }
}

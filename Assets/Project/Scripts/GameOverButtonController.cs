﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverButtonController : MonoBehaviour
{
    [Tooltip("The name of the level select scene.")]
    public string levelSelectScene = "LevelSelect";
    Button restartButton;
    Button goToLevelSelectButton;

    void Start()
    {
        restartButton = this.transform.root.GetComponentsInChildren<Button>(true)[0];
        goToLevelSelectButton = this.transform.root.GetComponentsInChildren<Button>(true)[1];
        //restartButton.Select();
    }

    public void Restart()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }

    public void GoToLevelSelect()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(levelSelectScene, LoadSceneMode.Single);   
    }
}

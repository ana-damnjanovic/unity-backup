// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "AVR/Generic_Surface"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		_Normal("Normal", 2D) = "bump" {}
		_NormalIntensity("Normal Intensity", Range( 0 , 10)) = 1
		_AO("AO", 2D) = "white" {}
		_AOStrength("AO Strength", Range( 0 , 1)) = 0.5
		_DirtMap("Dirt Map", 2D) = "white" {}
		_DirtSmall("Dirt Small ", Range( 0 , 1)) = 0
		_Smoothness("Smoothness", Range( 0 , 1)) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityStandardUtils.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float _NormalIntensity;
		uniform sampler2D _Normal;
		uniform float4 _Normal_ST;
		uniform float4 _Color;
		uniform float _Smoothness;
		uniform sampler2D _DirtMap;
		uniform float4 _DirtMap_ST;
		uniform float _DirtSmall;
		uniform sampler2D _AO;
		uniform float4 _AO_ST;
		uniform float _AOStrength;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_Normal = i.uv_texcoord * _Normal_ST.xy + _Normal_ST.zw;
			o.Normal = UnpackScaleNormal( tex2D( _Normal, uv_Normal ) ,_NormalIntensity );
			o.Albedo = _Color.rgb;
			float4 temp_cast_1 = (_Smoothness).xxxx;
			float2 uv_DirtMap = i.uv_texcoord * _DirtMap_ST.xy + _DirtMap_ST.zw;
			float2 uv_AO = i.uv_texcoord * _AO_ST.xy + _AO_ST.zw;
			float temp_output_18_0 = ( ( 1.0 - tex2D( _AO, uv_AO ).r ) * _AOStrength );
			float4 temp_cast_2 = (temp_output_18_0).xxxx;
			o.Smoothness = ( ( temp_cast_1 - ( tex2D( _DirtMap, uv_DirtMap ) * _DirtSmall ) ) - temp_cast_2 ).r;
			o.Occlusion = ( 1.0 - temp_output_18_0 );
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=14201
7;29;1906;1004;1584.436;288.2621;1.254811;True;True
Node;AmplifyShaderEditor.SamplerNode;17;-1167.022,629.9466;Float;True;Property;_AO;AO;3;0;Create;None;314fee6d5b86282489d8e34ce9b3f04f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;13;-1159.517,512.4592;Float;False;Property;_DirtSmall;Dirt Small ;6;0;Create;0;0.3;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-1166.608,274.5738;Float;True;Property;_DirtMap;Dirt Map;5;0;Create;None;68effaf9418fbbb43a50eee5cb9ae026;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;19;-1162.841,910.8605;Float;False;Property;_AOStrength;AO Strength;4;0;Create;0.5;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;22;-800.8914,660.5658;Float;False;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;12;-806.0948,373.4037;Float;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;11;-1145.803,59.89604;Float;False;Property;_Smoothness;Smoothness;7;0;Create;0;0.65;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-618.2201,761.9275;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-1557.147,-341.0602;Float;False;Property;_NormalIntensity;Normal Intensity;2;0;Create;1;2;0;10;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;10;-573.2824,91.71854;Float;False;2;0;FLOAT;0.0;False;1;COLOR;0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;23;-452.8914,658.5658;Float;False;1;0;FLOAT;0.0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;1;-1167.642,-387.2998;Float;True;Property;_Normal;Normal;1;0;Create;None;1dbb061dafe013747b6af9c7b76c342f;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;9;-695.5413,-604.8362;Float;False;Property;_Color;Color;0;0;Create;0,0,0,0;0.09558772,0.09558772,0.09558772,0;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;24;-342.173,94.45533;Float;False;2;0;COLOR;0.0;False;1;FLOAT;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;AVR/Generic_Surface;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;0;0;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;22;0;17;1
WireConnection;12;0;2;0
WireConnection;12;1;13;0
WireConnection;18;0;22;0
WireConnection;18;1;19;0
WireConnection;10;0;11;0
WireConnection;10;1;12;0
WireConnection;23;0;18;0
WireConnection;1;5;14;0
WireConnection;24;0;10;0
WireConnection;24;1;18;0
WireConnection;0;0;9;0
WireConnection;0;1;1;0
WireConnection;0;4;24;0
WireConnection;0;5;23;0
ASEEND*/
//CHKSM=97398F085D899C6B8417BF35CE15F657B11EA41A
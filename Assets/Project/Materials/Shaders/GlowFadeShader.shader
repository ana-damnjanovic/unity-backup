﻿Shader "Custom/GlowFadeShader" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		[HDR]_EmissionColor("Color", Color) = (0,0,0)
		_EmissionMap("Emission", 2D) = "white" {}
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf Standard

			sampler2D	_MainTex;
			half3		_EmissionColor;
			sampler2D   _EmissionMap;

			struct Input {
				float2 uv_MainTex;
			};

			void surf(Input IN, inout SurfaceOutputStandard o)
			{
				//half4 c = tex2D(_MainTex, IN.uv_MainTex);
				//half4 e = tex2D(_EmissionMap, IN.uv_MainTex);
				//half posSin = 0.5 * sin(40 * _Time.x) + 0.5;
				//half pulseMultiplier = posSin * (1 - _MinPulseVal) + _MinPulseVal;
				//o.Emission = e.rgb * _EmissionColor.rgb * 1/_Time.x;
				float intensity = lerp(0.603931, 0.25, _Time.x * 2);
				//o.Emission =  _EmissionColor.rgb * intensity;
				o.Emission = tex2D(_EmissionMap, IN.uv_MainTex).rgb * _EmissionColor.rgb * intensity;
				//o.Albedo = c.rgb;
				//o.Alpha = c.a;
			}
			ENDCG
		}
			FallBack "Standard"
}
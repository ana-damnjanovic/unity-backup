﻿using System.Collections;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class MSDragObjects : MonoBehaviour {

	public enum TypeSelect {Tag, Name, PhysicsMaterial};
	[Tooltip("The way the code will detect the objects, can be by tag, by name or by 'physics material'.")]
	public TypeSelect detectionMode = TypeSelect.Tag;
	[Header("Move and Rotate Objects")]
	[Tooltip("Uses controller joysticks to rotate when true (mouse controls by default)")]
	public bool useController = false;
	[Tooltip("The tag of objects that can be moved and rotated with the mouse.")]
	public string _tagObjects = "Destructible";
	[Tooltip("The tag of objects that can be moved and rotated with the mouse and will explode when thrown.")]
	public string _tagExplosiveObjects = "Explosive";
	[Tooltip("Objects in this layer can't be picked up even if they have the correct tag")]
	public LayerMask ignoreLayer;
	[Tooltip("The name of objects that can be moved and rotated with the mouse.")]
	public string _nameObjects = "Cube";
	[Tooltip("The 'physic material' of objects that can be moved and rotated with the mouse.")]
	public PhysicMaterial _physicMaterialObjects;
	//
	[Header("Move but not Rotate Objects")]
	[Tooltip("The tag of objects that can be moved with the mouse.")]
	public string tagObjects = "Blade";
	[Tooltip("The name of objects that can be moved with the mouse.")]
	public string nameObjects = "Cube";
	[Tooltip("The 'physic material' of objects that can be moved with the mouse.")]
	public PhysicMaterial physicMaterialObjects;

	//

	[Space(20)]
	[Tooltip("The key that must be pressed to move the objects.")]
	public KeyCode KeyToMove = KeyCode.Mouse0;
	[Tooltip("The key that must be pressed to throwing the objects.")]
	public KeyCode KeyToThrowing = KeyCode.Mouse1;
	[Tooltip("The key that must be pressed to rotate the objects.")]
	public KeyCode keyToRotate = KeyCode.R;

	[Space(15)][Tooltip("If this variable is checked, the mouse is automatically hidden.")]
	public bool hideTheMouse = false;
	[Tooltip("If this variable is true, the code will automatically adjust the player layer to 'IgnoreRaycast'")]
	public bool setLayerInPlayer = true;
	[Tooltip("Sets whether the cursor is free, locked and invisible or only limited.")]
	public CursorLockMode _cursorLockMode = CursorLockMode.None;

	public enum TypeMove {position, velocity, addForce};
	[Space(15),Tooltip("Here it is possible to decide the type of movement that the script will make to the rigid body of the objects that can be moved, so that the system adapts better to several situations.")]
	public TypeMove TypeOfMovement = TypeMove.velocity;
	[Tooltip("If this variable is true, the mass of the object being loaded will affect the speed at which it can be rotated.")]
	public bool massAffectsRotationalSpeed = true;
	[Range(1.0f,5.0f)][Tooltip("The closest you can bring your player's camera object.")]
	public float minDistance = 2.5f;
	[Range(1.0f,10.0f)][Tooltip("As far as you can bring your player's camera object.")]
	public float maxDistance = 6;
	[Range(1.0f,20.0f)][Tooltip("The speed at which the object can be moved.")]
	public float speedOfMovement = 5;
	[Range(5.0f,100.0f)][Tooltip("The speed at which the object can be rotated.")]
	public float speedOfRotation = 25;
	[Range(1.0f,30.0f)][Tooltip("The speed at which the object can be approached or disapproved.")]
	public float speedScrool =  10;
	[Tooltip("The force with which the object can be thrown.")]
	public float throwingForce = 800;
	[Tooltip("The force with which the object can be moved. This variable only takes effect if the selected motion type is 'AddForce'.")]
	public float moveForce = 350;
	[Space(15),Tooltip("A 'closed hand' texture, representing that the object is being moved.")]
	public Sprite closedHandTexture;
	[Tooltip("A 'open hand' texture, representing that the object can be moved.")]
	public Sprite openHandTexture;
	[Tooltip("A default texture, shown when cursor is not interacting with any movable object.")]
	public Sprite defaultHandTexture;
	[Range(0.1f,5.0f)][Tooltip("This variable will set the size with which the textures will appear on the screen.")]
	public float texturesScale = 1.0f;
	[Range(1f, 50f)]
	[Tooltip("This variable will set the explosion strength of thrown objects.")]
	public float explosivePower = 10f;
	[Tooltip("This prefab will be spawned when a thrown object explodes.")]
	public GameObject explosionPrefab;

	bool blockRotation;
	bool canMove;
	bool blockMovement;
	bool isMoving;
	bool isHoldingObject;
	bool objectThrown;
	bool canDrop;
	float distance;
	float rotXTemp;
	float rotYTemp;
	float tempDistance;
	float massFactor;
	RaycastHit tempHit;
	Rigidbody rbTemp;
	Vector3 rayEndPoint;
	Vector3 velocity = Vector3.zero;
	Vector3 tempDirection;
	Vector3 tempSpeed;
	Vector3 direcAddForceMode;
	Vector3 pointClosestToTheCollider;
	GameObject tempObject;
	public static bool rotatingObject;
	Camera mainCamera;
	PostProcessVolume postProcessVolume;
	DepthOfField depthOfField = null;
	GameObject objClosedHand;
	GameObject objOpenHand;
	GameObject objDefaultHand;
	ParticleSystem throwEffect;
	BladeSpawner bladeSpawner;
	CameraController cameraRotationControl;

	public Transform heldItemLocation;
	public Transform throwLocation;

	public bool canControlCameraRotation = true;

	void Awake (){
		distance = (minDistance + maxDistance)/2;
		mainCamera = this.gameObject.GetComponent<Camera>();
		if (!mainCamera)
        {
			mainCamera = Camera.main;
		}
		cameraRotationControl = this.transform.parent.gameObject.GetComponent<CameraController>();
		bladeSpawner = this.gameObject.GetComponent<BladeSpawner>();
		postProcessVolume = mainCamera.GetComponent<PostProcessVolume>();
		postProcessVolume.profile.TryGetSettings(out depthOfField);
		throwEffect = this.transform.GetComponentInChildren<ParticleSystem>();
		heldItemLocation = transform.Find("HeldItemLocation");
		throwLocation = transform.Find("ThrowLocation");
		isHoldingObject = false;
		objectThrown = false;
		canDrop = false;
		if (!mainCamera) {
			Debug.LogError ("The camera containing the code must have the 'MainCamera' tag so that you can rotate the objects.");
		}
		if (hideTheMouse) {
			Cursor.visible = false;
		}
		Cursor.lockState = _cursorLockMode;
		if (setLayerInPlayer) {
			GameObject refTemp = transform.root.gameObject;
			refTemp.layer = 2;
			foreach (Transform trans in refTemp.GetComponentsInChildren<Transform>(true)) {
				trans.gameObject.layer = 2;
			}
		}
		//
		float tempDistance = 0.3f;
		float tempScale = texturesScale * 0.05f;
		float tempfloatNear = mainCamera.nearClipPlane;
		if (tempfloatNear >= tempDistance) {
			tempDistance = tempfloatNear + 0.02f;
		}
		if (closedHandTexture) {
			objClosedHand = new GameObject ("objHandTextureClosed");
			objClosedHand.transform.parent = this.transform;
			objClosedHand.AddComponent<SpriteRenderer> ().sprite = closedHandTexture;
			objClosedHand.transform.localPosition = new Vector3 (0.0f, 0.0f, tempDistance);
			objClosedHand.transform.localScale = new Vector3 (tempScale, tempScale, tempScale);
			objClosedHand.transform.localRotation = Quaternion.identity;
			objClosedHand.SetActive (false);
		}
		if (openHandTexture) {
			objOpenHand = new GameObject ("objHandTextureOpen");
			objOpenHand.transform.parent = this.transform;
			objOpenHand.AddComponent<SpriteRenderer> ().sprite = openHandTexture;
			objOpenHand.transform.localPosition = new Vector3 (0.0f, 0.0f, tempDistance);
			objOpenHand.transform.localScale = new Vector3 (tempScale, tempScale, tempScale);
			objOpenHand.transform.localRotation = Quaternion.identity;
			objOpenHand.SetActive (false);
		}
		if (defaultHandTexture)
		{
			objDefaultHand = new GameObject("objHandTextureDefault");
			objDefaultHand.transform.parent = this.transform;
			objDefaultHand.AddComponent<SpriteRenderer>().sprite = defaultHandTexture;
			objDefaultHand.transform.localPosition = new Vector3(0.0f, 0.0f, tempDistance);
			objDefaultHand.transform.localScale = new Vector3(tempScale, tempScale, tempScale);
			objDefaultHand.transform.localRotation = Quaternion.identity;
			objDefaultHand.SetActive(false);
		}
	}

	void RaycastVector3Down(){//raycast to check if the player is stepping on top of the object he is moving. If so, the code will block the movement of objects.
		if (tempObject) {
			if (Physics.Raycast (transform.position, Vector3.down, out tempHit, 5, ~ignoreLayer)) {
				switch (detectionMode) {
				case TypeSelect.Tag:
					if (tempHit.transform.CompareTag(_tagObjects) && tempObject.transform.gameObject == tempHit.transform.gameObject) {
						blockMovement = true;
					} 
					else if (tempHit.transform.CompareTag(_tagExplosiveObjects) && tempObject.transform.gameObject == tempHit.transform.gameObject) {
						blockMovement = true;
					}
					else if (tempHit.transform.CompareTag(tagObjects) && tempObject.transform.gameObject == tempHit.transform.gameObject) {
						blockMovement = true;
					} 
					else {
						blockMovement = false;
					}
					break;
				case TypeSelect.Name:
					if (tempHit.transform.name == _nameObjects && tempObject.transform.gameObject == tempHit.transform.gameObject) {
						blockMovement = true;
					} 
					else if (tempHit.transform.name == nameObjects && tempObject.transform.gameObject == tempHit.transform.gameObject) {
						blockMovement = true;
					} 
					else {
						blockMovement = false;
					}
					break;
				case TypeSelect.PhysicsMaterial:
					if (_physicMaterialObjects) {
						if (tempHit.transform.GetComponent<Collider> ().sharedMaterial == _physicMaterialObjects && tempObject.transform.gameObject == tempHit.transform.gameObject) {
							blockMovement = true;
						} 
						else if (tempHit.transform.GetComponent<Collider> ().sharedMaterial == physicMaterialObjects && tempObject.transform.gameObject == tempHit.transform.gameObject) {
							blockMovement = true;
						} 
						else {
							blockMovement = false;
						}
					} else {
						blockMovement = false;
					}
					break;
				}
			} else {
				blockMovement = false;
			}
		} else {
			blockMovement = false;
		}
	}

	void Update (){
		RaycastVector3Down ();
		//raycast camera forward
		rayEndPoint = transform.position + transform.forward*distance;
		if (Physics.Raycast (transform.position, transform.forward, out tempHit, (maxDistance + 1), ~ignoreLayer)) {
			switch (detectionMode) {
			case TypeSelect.Tag:
				if (Vector3.Distance (transform.position, tempHit.point) <= maxDistance && tempHit.transform.CompareTag(_tagObjects)) {
					canMove = true;
					if (!isMoving)
					{
						blockRotation = false;
					}
					break;
				} 
				if (Vector3.Distance(transform.position, tempHit.point) <= maxDistance && tempHit.transform.CompareTag(_tagExplosiveObjects)) { 
					canMove = true;
					if (!isMoving) {
						blockRotation = false;
					}
					break;
				}
				if (Vector3.Distance (transform.position, tempHit.point) <= maxDistance && tempHit.transform.CompareTag(tagObjects)) {
					canMove = true;
					if (!isMoving) {
						blockRotation = true;
					}
					break;
				} 
				canMove = false;
				if (!isMoving) {
					blockRotation = false;
				}
				break;
			case TypeSelect.Name:
				if (Vector3.Distance (transform.position, tempHit.point) <= maxDistance && tempHit.transform.name == _nameObjects) {
					canMove = true;
					if (!isMoving) {
						blockRotation = false;
					}
					break;
				}
				if (Vector3.Distance (transform.position, tempHit.point) <= maxDistance && tempHit.transform.name == nameObjects) {
					canMove = true;
					if (!isMoving) {
						blockRotation = true;
					}
					break;
				}
				canMove = false;
				if (!isMoving) {
					blockRotation = false;
				}
				break;
			case TypeSelect.PhysicsMaterial:
				if (_physicMaterialObjects) {
					if (Vector3.Distance (transform.position, tempHit.point) <= maxDistance && tempHit.transform.GetComponent<Collider> ().sharedMaterial == _physicMaterialObjects) {
						canMove = true;
						if (!isMoving) {
							blockRotation = false;
						}
						break;
					} 
					if (Vector3.Distance (transform.position, tempHit.point) <= maxDistance && tempHit.transform.GetComponent<Collider> ().sharedMaterial == physicMaterialObjects) {
						canMove = true;
						if (!isMoving) {
							blockRotation = true;
						}
						break;
					}
					canMove = false;
					if (!isMoving) {
						blockRotation = false;
					}
				} else {
					canMove = false;
				}
				break;
			}
			if (Input.GetKeyDown(KeyToMove) && canMove) 
			{
				if (tempHit.transform.gameObject.tag == "Blade")
				{
					tempHit.transform.gameObject.GetComponent<BladeBehaviour>().PickUp();
					bladeSpawner.DecreaseNumBlades();
				}
				else if (!isHoldingObject)
                {
					isHoldingObject = true;
					bladeSpawner.SetCanSpawn(false);
					if (tempHit.rigidbody)
					{
						switch (TypeOfMovement)
						{
							case TypeMove.position:
								tempHit.rigidbody.useGravity = false;
								break;
							case TypeMove.velocity:
								tempHit.rigidbody.useGravity = true;
								break;
							case TypeMove.addForce:
								tempHit.rigidbody.useGravity = true;
								break;
						}
						distance = Vector3.Distance(transform.position, tempHit.point);
						tempObject = tempHit.transform.gameObject;
						isMoving = true;
						if (tempObject.tag == "Destructible")
						{
							if (tempObject.layer == ~ignoreLayer)
                            {
								blockMovement = true;
                            }
							else
                            {
								if (tempObject.GetComponent<RayFire.RayfireRigid>())
                                {
									tempObject.GetComponent<RayFire.RayfireRigid>().enabled = false;
								}
								rbTemp = tempObject.GetComponent<Rigidbody>();
								rbTemp.collisionDetectionMode = CollisionDetectionMode.Continuous;
                                rbTemp.interpolation = RigidbodyInterpolation.Interpolate;
                                if (rbTemp.mass > 1)
                                {
                                    rbTemp.mass = 1;
                                }
                            }
						}
					}
					else
					{
						Debug.LogWarning("The target object does not have the 'Rigidbody' component. This way, you can not interact with it.");
					}
				} 
			}
		} else {
			canMove = false;
			if (!tempObject) // object was destroyed (exploded) while being held
            {
				isHoldingObject = false;
				isMoving = false;
				if (!bladeSpawner.GetCanSpawn())
                {
					bladeSpawner.SetCanSpawn(true);
				}
			}
		}

		if (tempObject) {
			tempObject.transform.forward = transform.forward;
			rbTemp = tempObject.GetComponent<Rigidbody> ();
			pointClosestToTheCollider = tempObject.GetComponentInChildren<Collider>().ClosestPointOnBounds(transform.position);
			distance = Mathf.Max(minDistance, Vector3.Distance(transform.position, pointClosestToTheCollider));
			ParentAndDisableColliders(tempObject);
		}

		if (Input.GetKeyUp(KeyToMove) && isHoldingObject)
        {
			canDrop = true;
        }

		if (Input.GetKeyDown(KeyToMove) && canDrop)
		{ // player has clicked again and dropped the object
			isHoldingObject = false;
			isMoving = false;
			bladeSpawner.SetCanSpawn(true);
			if (tempObject)
			{
				rbTemp.useGravity = true;
				UnParentAndEnableColliders(tempObject);
				tempObject = null;
				rbTemp = null;
			}
			canDrop = false;
		}
		distance = Mathf.Clamp(distance, minDistance, maxDistance);

		if (blockMovement && tempObject) {
			rbTemp.useGravity = true;
			tempObject = null;
			rbTemp = null;
			isMoving = false;
		}
		if (Input.GetKeyDown(KeyToThrowing) && tempObject) {
			objectThrown = true;
			throwEffect.Play();
			if (tempObject.tag == "Explosive") {
				Explosive explosive = tempObject.AddComponent(typeof(Explosive)) as Explosive;
				explosive.power = explosivePower;
				explosive.explosionPrefab = explosionPrefab;
			}
			if (canControlCameraRotation)
			{
				cameraRotationControl.freezeCameraRotation = false;
			}
			tempDirection = rayEndPoint - transform.position;
			tempDirection.Normalize ();
			UnParentAndEnableColliders(tempObject);
			tempObject.transform.position = throwLocation.position;
			rbTemp.AddForce(tempDirection * throwingForce * 4);
			rbTemp.useGravity = true;
			tempObject = null;
			rbTemp = null;
			isMoving = false;
		}
		if (Input.GetKeyUp(KeyToThrowing) && objectThrown)
        {
			bladeSpawner.SetCanSpawn(true);
			objectThrown = false;
		}
		if (tempObject) {
			pointClosestToTheCollider = tempObject.GetComponentInChildren<Collider> ().ClosestPointOnBounds (transform.position);
			if (Vector3.Distance (transform.position, pointClosestToTheCollider) > maxDistance) {
				rbTemp.useGravity = true;
				tempObject = null;
				rbTemp = null;
				isMoving = false;
			}
		}

		if (tempObject && mainCamera && !blockRotation) {
			if (Input.GetKey (keyToRotate)) {
				if (massAffectsRotationalSpeed) {
					massFactor = Mathf.Clamp ((1.0f / rbTemp.mass), 0.01f, 0.3f);
				} else {
					massFactor = 0.2f;
				}
				rotatingObject = true;
				if (depthOfField)
                {
					depthOfField.enabled.value = true;
				}
				if (useController)
                {
					rotXTemp = Input.GetAxis("Axis 4") * speedOfRotation * massFactor;
					rotYTemp = Input.GetAxis("Axis 5") * speedOfRotation * massFactor;
				} 
				else
                {
					rotXTemp = Input.GetAxis("Mouse X") * speedOfRotation * massFactor;
					rotYTemp = Input.GetAxis("Mouse Y") * speedOfRotation * massFactor;

				}
				if (canControlCameraRotation)
                {
					cameraRotationControl.freezeCameraRotation = true;
				}
				tempObject.transform.Rotate (mainCamera.transform.up, -rotXTemp, Space.World);
				tempObject.transform.Rotate (mainCamera.transform.right, rotYTemp, Space.World);
			}
			if (Input.GetKeyUp (keyToRotate)) {
				rotatingObject = false;
				if (canControlCameraRotation)
                {
					cameraRotationControl.freezeCameraRotation = false;
				}
			}
		} else {
			rotatingObject = false;
		}
		if (!rotatingObject)
        {
            if (depthOfField)
            {
				depthOfField.enabled.value = false;
			}
		}
		//sprite elements
		if (canMove && !isMoving && openHandTexture) {
			objClosedHand.SetActive(false);
			objOpenHand.SetActive(true);
			objDefaultHand.SetActive(false);
		} 
		else if (isHoldingObject && isMoving && closedHandTexture) {
			objClosedHand.SetActive (true);
			objOpenHand.SetActive (false);
			objDefaultHand.SetActive(false);
		} 
		else {
			objClosedHand.SetActive (false);
			objOpenHand.SetActive (false);
			objDefaultHand.SetActive(true);
		}
	}

	void FixedUpdate (){
		if (tempObject) {
			rbTemp = tempObject.GetComponent<Rigidbody> ();
			rbTemp.angularVelocity = new Vector3 (0, 0, 0);
			massFactor = 1 / rbTemp.mass;
			switch (TypeOfMovement) {
			case TypeMove.position:
				rbTemp.velocity = new Vector3 (0, 0, 0);
				rbTemp.position = Vector3.SmoothDamp (tempObject.transform.position, heldItemLocation.position, ref velocity, (1/speedOfMovement));
				break;
			case TypeMove.velocity:
				tempSpeed = (heldItemLocation.position - rbTemp.transform.position);
				tempSpeed.Normalize ();
				tempDistance = Vector3.Distance (heldItemLocation.position, rbTemp.transform.position);
				tempDistance = Mathf.Clamp (tempDistance, 0, 1);
				rbTemp.velocity = Vector3.Lerp(rbTemp.velocity, tempSpeed*speedOfMovement*6.0f*tempDistance * massFactor,Time.deltaTime*15);
				break;
			case TypeMove.addForce:
				tempSpeed = (heldItemLocation.position - rbTemp.transform.position);
				tempSpeed.Normalize ();
				tempDistance = Vector3.Distance (heldItemLocation.position, rbTemp.transform.position);
				tempDistance = Mathf.Clamp (tempDistance, 0, 1);
				direcAddForceMode = tempSpeed * speedOfMovement * moveForce * tempDistance;

				rbTemp.velocity = Vector3.zero;
				rbTemp.AddForce (direcAddForceMode, ForceMode.Force);
				break;
			}
		}
	}
	void ParentAndDisableColliders(GameObject tempObject)
	{
		tempObject.transform.parent = heldItemLocation;
		tempObject.transform.position = heldItemLocation.position;
		Collider coll = tempObject.GetComponent<Collider>();
		if (coll) {
			coll.enabled = false;
        }
	}

	void OnDrawGizmos()
	{
		// Draws a 5 unit long red line in front of the object
		Gizmos.color = Color.red;
		Vector3 direction = transform.TransformDirection(Vector3.forward);
		Gizmos.DrawRay(transform.position, direction * maxDistance);
	}

	void UnParentAndEnableColliders(GameObject tempObject)
	{
		tempObject.transform.rotation = Quaternion.Euler(0, tempObject.transform.eulerAngles.y, 0);
		tempObject.transform.parent = null;
		Collider coll = tempObject.GetComponent<Collider>();
		if (coll)
		{
			coll.enabled = true;
		}
	}
}